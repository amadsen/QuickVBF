#pragma once //-*- mode:C++ -*- 

#include <string>

struct SysID {
  explicit SysID(const std::string& value) : value_(value) {}
  SysID(const SysID& id) : value_(id.value_) { }
  operator const std::string&() const { return value_; }
  bool operator==(const SysID& other) const { return value_ == other.value_; }
  bool operator<(const SysID& other) const { return value_ < other.value_; }
private:
  std::string value_;
};

struct FlowID {
  explicit FlowID(const std::string& value) : value_(value) {}
  FlowID(const FlowID& id) : value_(id.value_) { }
  operator const std::string&() const { return value_; }
  bool operator==(const FlowID& other) const { return value_ == other.value_; }
private:
  std::string value_;
};

struct VarID {
  explicit VarID(const std::string& value) : value_(value) {}
  VarID(const VarID& id) : value_(id.value_) { }
  operator const std::string&() const { return value_; }
  bool operator==(const VarID& other) const { return value_ == other.value_; }
private:
  std::string value_;
};
