#pragma once //-*- mode:C++ -*- 

#include <EventLoop/Algorithm.h>
#include <QuickVBF/QVBFConfig.h>
#include <memory>

class QuickVBF : public EL::Algorithm { 
public:
  void testInvariant() const;
  QuickVBF(const QVBFConfig cfg = QVBFConfig());
  ~QuickVBF();
  QuickVBF(QuickVBF&&) = delete;
  QuickVBF& operator=(QuickVBF&&) = delete; 
  virtual EL::StatusCode setupJob(EL::Job& job);
  virtual EL::StatusCode fileExecute();
  virtual EL::StatusCode histInitialize();
  virtual EL::StatusCode changeInput(bool firstFile);
  virtual EL::StatusCode initialize();
  virtual EL::StatusCode execute();
  virtual EL::StatusCode postExecute();
  virtual EL::StatusCode finalize();
  virtual EL::StatusCode histFinalize();
  QVBFConfig cfg;
private:
  struct Impl;
  const std::unique_ptr<Impl> p_; //!  
  ClassDef(QuickVBF, 1);
};
