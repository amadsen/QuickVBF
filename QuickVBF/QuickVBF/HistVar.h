#pragma once //-*- mode:C++ -*-

#include <Rtypes.h>
#include <functional>
#include <string>
#include <vector>

class WB;

struct Unit {
  std::string label;
  double value;
};

struct HistVar {
  std::string name;
  std::string title;
  std::vector<Double_t> bins;
  Unit unit;
  std::function<double(const WB&)> calc;
  
  HistVar(const std::string name,
	  const std::string title,
	  const std::vector<Double_t> bins,
	  const Unit unit,
	  const std::function<double(const WB&)> calc);
  
  HistVar(const std::string name,
	  const std::string title,
	  const unsigned n, const Double_t min, const Double_t max,
	  const Unit unit,
	  const std::function<double(const WB&)> calc);
  
  HistVar(const std::string name,
	  const std::string title,
	  const Double_t min, const Double_t max,
	  const Unit unit,
	  const std::function<double(const WB&)> calc);
};

