#pragma once //-*- mode:C++ -*- 

#include <QuickVBF/global.h>
#include <memory>

class TH1;

class HistStore {
 public:

  void set(const SysID sys, const FlowID flow, const VarID var, TH1* h);
  TH1* get(const SysID sys, const FlowID flow, const VarID var); 
  TH1* release(const SysID sys, const FlowID flow, const VarID var);
  
  HistStore();
  ~HistStore();
 private:
  struct Impl;
  const std::unique_ptr<Impl> p_;
};
