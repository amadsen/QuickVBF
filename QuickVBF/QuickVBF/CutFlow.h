#pragma once //-*- mode:C++ -*-

#include <QuickVBF/HistVar.h>
#include <QuickVBF/global.h>
#include <functional>
#include <string>
#include <vector>
#include <memory>

class HistStore;
class WB;

struct IFlowItem {
  virtual std::string label() const = 0;
  virtual double apply(const WB& wb) const = 0; 
  virtual ~IFlowItem() = default; 
};

struct Cut : IFlowItem {
  std::string lbl;
  std::function<bool(const WB&)> f;
  std::string label() const { return lbl; }
  double apply(const WB& wb) const { return (f(wb) ? 1. : 0.); }
  Cut(std::string lbl, std::function<bool(const WB&)> f) : lbl(std::move(lbl)), f(std::move(f)) {}
};

struct Wgt : IFlowItem {
  std::string lbl;
  std::function<double(const WB&)> f;
  std::string label() const { return lbl; }
  double apply(const WB& wb) const { return f(wb); }
  Wgt(std::string lbl, std::function<double(const WB&)> f) : lbl(std::move(lbl)), f(std::move(f)) {}
};

struct Flow {
  std::string id;
  std::vector<std::shared_ptr<IFlowItem>> items;
  std::vector<HistVar> hists;
  bool writeNtuple = false;
  bool systematics = true;
  Flow(std::string id, std::vector<std::shared_ptr<IFlowItem>> items, std::vector<HistVar> hists)
    : id(std::move(id)), items(std::move(items)), hists(std::move(hists)) {
  }
  Flow(std::string id, std::vector<std::shared_ptr<IFlowItem>> items)
    : id(std::move(id)), items(std::move(items)) {
  }
  Flow(std::string id) : id(std::move(id)) {}
};

void prepare(const SysID& sysID, const Flow& flow, HistStore& histStore);

bool process(const SysID& sysID, const Flow& flow, const WB& wb, HistStore& histStore);
