#pragma once //-*- mode:C++ -*-

#include <QuickVBF/HistVar.h>
#include <vector>

namespace VBFHists {
  std::vector<HistVar> nV();
  std::vector<HistVar> Zee();
  std::vector<HistVar> Zmm();
  std::vector<HistVar> Wev();
  std::vector<HistVar> Wmv();
  std::vector<HistVar> JJ();
  std::vector<HistVar> nEvt();
  std::vector<HistVar> allHists();
}
