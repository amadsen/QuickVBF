#pragma once //-*- mode:C++ -*-

#include <RootCoreUtils/Assert.h>
#include <xAODRootAccess/TEvent.h>

template<typename T>
static T const * safeRetrieve(xAOD::TEvent* event, const std::string& key) {
  RCU_REQUIRE( event );
  RCU_REQUIRE( not key.empty() );
  T const * object = nullptr;
  if ( not event->retrieve( object, key ).isSuccess() ) {
    const std::string msg = "Failed to retrieve " + key;
    Error("safeRetrieve()", msg.c_str());
    throw std::runtime_error(msg);
  }
  RCU_PROVIDE( object );
  return object;
}
