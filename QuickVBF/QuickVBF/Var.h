#pragma once //-*- mode:C++ -*- 

#include <TTree.h>
#include <vector>

struct IBranchHandle {
  virtual void clear() = 0;
  virtual void connect(TTree* t) const = 0;
};


struct BranchHandler {
  std::vector<IBranchHandle*> branches;
};
void clear(BranchHandler& bh);
void connect(const BranchHandler& bh, TTree* tree);
 

template<typename T>
class Variable : public IBranchHandle {

public:

  Variable() = default;

  Variable(BranchHandler* owner, std::string name)
    : name_(std::move(name)) {
    owner->branches.push_back(this);
  }

  void operator()(T val) {
    val_ = val;
  }

  T operator()() const {
    return val_;
  }

  void clear() {
    val_ = T();
  }

  void connect(TTree* t) const {
    t->Branch(name_.c_str(), const_cast<T*>(&val_));
  }

private:

  T val_;
  std::string name_;
};


template<typename T>
class Vector : public IBranchHandle {

  using V = std::vector<T>;

public:

  Vector() = default;

  Vector(BranchHandler* owner, std::string name)
    : name_(std::move(name)) {
    owner->branches.push_back(this);
  }

  V& operator()() {
    return val_;
  }

  void clear() {
    val_.clear();
  }

  void connect(TTree* t) const {
    t->Branch(name_.c_str(), const_cast<V*>(&val_));
  }

private:

  V val_;
  std::string name_;
};

