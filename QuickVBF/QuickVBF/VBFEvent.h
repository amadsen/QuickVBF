#pragma once //-*- mode:C++ -*-

#include <QuickAna/QuickAna.h>
#include <TLorentzVector.h>
#include <TVector2.h>
#include <map>
#include <string>
#include <vector>

struct VBFEvent {
  using QAMET      = decltype(std::declval<ana::IQuickAna>().met());

  ConstDataVector<xAOD::ElectronContainer> electrons;
  ConstDataVector<xAOD::MuonContainer> muons;
  ConstDataVector<xAOD::JetContainer> jets;
  
  TVector2 met;
  TVector2 met2;
  double met_jet_dphi = 0;
  TVector2 lepmet;
  double lepmet_jet_dphi = 0;
  TLorentzVector z;
  TLorentzVector jj;
  double jj_dphi = -1;
  double jj_deta = -1;
  double other_central_jet_pt = 0;
  double other_forward_jet_pt = 0;
  double met_lep_mt = 0;
  double true_electron_pt = 0;
  double true_muon_pt = 0;
  double true_tau_pt = 0;

  TVector2 mhlt;
  double mhlt_lep_mt = 0;
  double jj_jet_dphi = 0;
  double minusjj_jet_dphi = 0;
  double mhlt_jet_dphi = 0;

  std::map<std::string, bool> triggers; 
};

std::shared_ptr<VBFEvent> analyze(xAOD::TEvent& xv, ana::IQuickAna& qa);
