#pragma once //-*- mode:C++ -*-

#include <QuickVBF/global.h>
#include <QuickVBF/VBFEvent.h>
#include <QuickVBF/VBFWeights.h>
#include <memory>

struct WB {
  xAOD::TEvent* xAODEvent = nullptr;
  const xAOD::EventInfo * ei = nullptr;
  ana::IQuickAna* qa = nullptr;
  std::shared_ptr<const VBFEvent> vbfEvent;
  std::shared_ptr<const VBFWeights> vbfWeights; 
  int nPV = -1;
  int nV = -1;
  bool passGRL = false;
  SysID systematic{"notset"};
};
