#pragma once //-*- mode:C++ -*- 

#include <QuickVBF/WB.h>

struct VBFNtuple {
  VBFNtuple();
  ~VBFNtuple();
  void fill(const WB& wb);
  void setTree(TTree* tree);
private:
  struct Impl;
  const std::unique_ptr<Impl> p_; 
};

