#pragma once //-*- mode:C++ -*- 

#include <QuickVBF/WB.h>
#include <memory>

class VBFNtuple;
class TTree;

class NtupleWriter {
public:
  NtupleWriter(VBFNtuple*);
  ~NtupleWriter();
  void initialize(TTree* tree);
  void process(const WB& wb);
private:
  struct Impl;
  const std::unique_ptr<Impl> p_; 
};


