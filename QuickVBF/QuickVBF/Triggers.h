#pragma once //-*- mode:C++ -*-

#include <string>

namespace xAOD { class TEvent; }
namespace ana { class IQuickAna; }

bool passTrigger(const std::string& trigger, xAOD::TEvent& xv, ana::IQuickAna& qa);
