
#pragma once //-*- mode:C++ -*- 

#include <cassert>
#include <iterator>
#include <memory>
#include <vector>

namespace astd {

  template<typename T, typename... Args>
  std::unique_ptr<T> make_unique(Args&&... args) {
    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
  }

  template<typename T>
  T& ref(T* p) {
    assert( p );
    return *p;
  }

  template<typename T>
  void move_items(std::vector<T>&& from, std::vector<T>& to) {
    to.insert(to.end(),
	      std::make_move_iterator(from.begin()),
	      std::make_move_iterator(from.end()));
  }

  template<class C1, class C2>
  void insert(C1& c1, const C2& c2) {
    c1.insert(c2.begin(), c2.end());
  }
}
