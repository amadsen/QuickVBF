#pragma once //-*- mode:C++ -*-

#include <QuickVBF/global.h>
#include <memory>

namespace xAOD { class TEvent; }
namespace ana { class IQuickAna; }
class VBFEvent;

struct VBFWeights {
  bool elTrig = false;
  bool e2Trig = false;
  bool muTrig = false;
  bool xeTrig = false;
  double weightElTrigSF = 0.;
  double weightE2TrigSF = 0.;
  double weightMuTrigSF = 0.;
  double weightXETrigSF = 0.;
  double weightReco = 0.;
  double weightPileup = 0.;
  double weightNJet = 0.;
  double weightMC = 0.;
};

std::shared_ptr<VBFWeights> calcWeights(xAOD::TEvent& xv, ana::IQuickAna& qa,
					const VBFEvent& vbfEvent, const SysID sysID);
