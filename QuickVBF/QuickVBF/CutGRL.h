#pragma once //-*- mode:C++ -*- 

#include <memory>
#include <string>

class QVBFConfig;
class WB;

class CutGRL {
public:
  CutGRL();
  ~CutGRL();
  std::string name() const; 
  void initialize(const QVBFConfig&);  
  bool pass(const WB&) const;
private:
  struct Impl;
  const std::unique_ptr<Impl> p_; 
};
