#pragma once //-*- mode:C++ -*- 

#include <QuickAna/Configuration.h>
#include <Rtypes.h>
#include <string>
#include <vector>

struct QVBFConfig { 
  std::vector<std::string> dirGRL;
  std::string dirPRW;
  ana::Configuration qaConf;
  bool writeNtupleNominal = true;
  bool writeNtupleSystematics = true;
  bool use2jPreSelection = false;
  QVBFConfig() = default;
  virtual ~QVBFConfig() = default;
  ClassDef(QVBFConfig, 1);
};
