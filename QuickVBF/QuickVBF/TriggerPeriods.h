#pragma once //-*- mode:C++ -*-

#include <set>
#include <string>
#include <vector>

int triggerPeriodCategory(const unsigned runNumber);

std::set<std::string> triggers2015();

std::set<std::string> triggers2016();

std::set<std::string> triggers20152016();

std::vector<std::string> elTrigger(const unsigned runNumber);

std::vector<std::string> e2Trigger(const unsigned runNumber);

std::vector<std::string> muTrigger(const unsigned runNumber);

std::vector<std::string> metTrigger(const unsigned runNumber);
