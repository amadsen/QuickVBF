#include <QuickVBF/VBFEvent.h>
#include <QuickVBF/safeRetrieve.h>
#include <QuickVBF/TriggerPeriods.h>
#include <QuickVBF/Triggers.h>
#include <TMath.h>
#include <cassert>
#include <iostream>

#include <xAODTruth/TruthParticleContainer.h>

// static double truth_electron_pt(xAOD::TEvent& xv) {
//   auto const * const truthparticles = safeRetrieve<xAOD::TruthParticleContainer>(&xv, "TruthParticles");  
//   xAOD::TruthParticleContainer::const_iterator truth_part_itr = truthparticles->begin(); 
//   xAOD::TruthParticleContainer::const_iterator truth_part_end = truthparticles->end();
//   double true_lepton_pt = 0;
//   for( ; truth_part_itr != truth_part_end; ++truth_part_itr ) {
//     if ( not ((*truth_part_itr)->status() == 1) ) {
//       continue;
//     }
//     if (TMath::Abs( (*truth_part_itr)->pdgId() ) != 11) { 
//       continue;
//     }
//     const double absEta = TMath::Abs((*truth_part_itr)->eta());
//     if (absEta < 1.37 || (absEta > 1.52 && absEta < 2.47)) {
//       const double pt = (*truth_part_itr)->pt();
//       if (pt > 0 && pt < 10000000 && pt > true_lepton_pt) {
// 	true_lepton_pt = pt;
//       } 
//     }
//   }
//   return true_lepton_pt;
// }

static double true_electron_pt(xAOD::TEvent& xv) {
  auto const * const truth
    = safeRetrieve<xAOD::TruthParticleContainer>(&xv, "TruthParticles");
  double max_pt = 0;
  for (auto it = truth->begin(); it != truth->end(); ++it) {
    if ( not ((*it)->status() == 1) ) { continue; }
    if (TMath::Abs((*it)->pdgId()) != 11) { continue; }
    const double absEta = TMath::Abs((*it)->eta());
    if (absEta < 1.37 || (absEta > 1.52 && absEta < 2.47)) {
      const double pt = (*it)->pt();
      if (pt > 0 && pt < 10000000 && pt > max_pt) {
	max_pt = pt;
      } 
    }
  }
  return max_pt;
}

static double true_muon_pt(xAOD::TEvent& xv) {
  auto const * const truth
    = safeRetrieve<xAOD::TruthParticleContainer>(&xv, "TruthParticles");
  double max_pt = 0;
  for (auto it = truth->begin(); it != truth->end(); ++it) {
    if ( not ((*it)->status() == 1) ) { continue; }
    if (TMath::Abs((*it)->pdgId()) != 13) { continue; }
    if (TMath::Abs((*it)->eta()) < 2.5) {
      const double pt = (*it)->pt();
      if (pt > 0 && pt < 10000000 && pt > max_pt) {
	max_pt = pt;
      } 
    }
  }
  return max_pt;
}

static double true_tau_pt(xAOD::TEvent& xv) {
  auto const * const truth
    = safeRetrieve<xAOD::TruthParticleContainer>(&xv, "TruthParticles");
  double max_pt = 0;
  for (auto it = truth->begin(); it != truth->end(); ++it) {
    if ( not ((*it)->status() == 1) ) { continue; }
    if (TMath::Abs((*it)->pdgId()) != 15) { continue; }
    if (TMath::Abs((*it)->eta()) < 2.47) {
      const double pt = (*it)->pt();
      if (pt > 0 && pt < 10000000 && pt > max_pt) {
	max_pt = pt;
      } 
    }
  }
  return max_pt;
}

static double deltaPhi(const double phi1, const double phi2) {
  double dPhi = phi1 - phi2;
  while (dPhi >= TMath::Pi()) dPhi -= 2. * TMath::Pi();
  while (dPhi < -TMath::Pi()) dPhi += 2. * TMath::Pi();
  return TMath::Abs(dPhi);
}

static double deltaEta(const double eta1, const double eta2) {
  return TMath::Abs(eta1 - eta2);
}

static double MT(const double pt1, const double phi1,
		 const double pt2, const double phi2) {
  const double dPhi = deltaPhi(phi1, phi2);
  return TMath::Sqrt( 2. * pt1 * pt2 * ( 1. - TMath::Cos(dPhi) ) );
}

static double MT(const ConstDataVector<xAOD::ElectronContainer>& el,
		 const ConstDataVector<xAOD::MuonContainer>& mu,
		 const VBFEvent::QAMET& met) {
  const double mte = (el.size() == 0 ? 0 : MT(el[0]->pt(), el[0]->phi(), met->met(),  met->phi()));
  const double mtm = (mu.size() == 0 ? 0 : MT(mu[0]->pt(), mu[0]->phi(), met->met(),  met->phi()));
  return (mte > mtm ? mte : mtm);  
}

static double MT(const ConstDataVector<xAOD::ElectronContainer>& el,
		 const ConstDataVector<xAOD::MuonContainer>& mu,
		 const TVector2& met) {
  const double mte = (el.size() == 0 ? 0 : MT(el[0]->pt(), el[0]->phi(), met.Mod(),  met.Phi()));
  const double mtm = (mu.size() == 0 ? 0 : MT(mu[0]->pt(), mu[0]->phi(), met.Mod(),  met.Phi()));
  return (mte > mtm ? mte : mtm);  
}

static TLorentzVector Zll(const ConstDataVector<xAOD::ElectronContainer>& el,
			  const ConstDataVector<xAOD::MuonContainer>& mu) {
  TLorentzVector z;
  if (el.size() >= 2) {
    if (el[0]->charge() * el[1]->charge() < 0) {
      z = el[0]->p4() + el[1]->p4();
    }
  }
  if (mu.size() >= 2) {
    if (mu[0]->charge() * mu[1]->charge() < 0) {
      z = mu[0]->p4() + mu[1]->p4();
    }
  }
  return z;
}

static TVector2 lepMet(const ConstDataVector<xAOD::ElectronContainer>& el,
		       const ConstDataVector<xAOD::MuonContainer>& mu,
		       const VBFEvent::QAMET& met) {
  TVector2 lepmet;
  lepmet.SetMagPhi( met->met(), met->phi() );
  for (const auto& e : el) {
    TVector2 v;
    v.SetMagPhi(e->pt(), e->phi());
    lepmet += v;
  }
  for (const auto& m : mu) {
    TVector2 v;
    v.SetMagPhi(m->pt(), m->phi());
    lepmet += v;
  }
  return lepmet;
}

static TVector2 transverse(const TLorentzVector& p4) {
  TVector2 proj;
  proj.SetMagPhi(p4.Pt(), p4.Phi());
  return proj;
}  

static TVector2 MHLT(const ConstDataVector<xAOD::ElectronContainer>& el,
		     const ConstDataVector<xAOD::MuonContainer>& mu,
		     const ConstDataVector<xAOD::JetContainer>& jet) {
  TLorentzVector mhl;
  for (const auto& j : jet) { mhl -= j->p4(); }
  for (const auto& e :  el) { mhl -= e->p4(); }
  for (const auto& m :  mu) { mhl -= m->p4(); }
  return transverse(mhl);
}

static double minDPhi(const double phi1,const ConstDataVector<xAOD::JetContainer>& jet) {
  double min_dPhi = 999;
  for (const auto& j : jet) {
    const double dPhi = deltaPhi(phi1, j->phi());
    if (dPhi < min_dPhi) {
      min_dPhi = dPhi;
    }
  }
  return min_dPhi;
}

template<class T>
static void sort_particles(ConstDataVector<DataVector<T>>& v) {
  std::sort(v.begin(), v.end(), [](T* a, T* b) { return b->pt() < a->pt(); });
}

std::shared_ptr<VBFEvent> analyze(xAOD::TEvent& xv, ana::IQuickAna& qa) {
  std::shared_ptr<VBFEvent> evptr(new VBFEvent);
  VBFEvent& ev = *evptr.get();

  ev.electrons = ConstDataVector<xAOD::ElectronContainer>(SG::VIEW_ELEMENTS);
  for (auto electron : *qa.electrons()) {
    if (electron->auxdata<ana::SelectType>("ana_select")) {
      ev.electrons.push_back( electron );
    }
  }
  sort_particles(ev.electrons);
  ev.muons = ConstDataVector<xAOD::MuonContainer>(SG::VIEW_ELEMENTS);
  for (auto muon : *qa.muons()) {
    if (muon->auxdata<ana::SelectType>("ana_select")) {
      ev.muons.push_back( muon );
    }
  }
  sort_particles(ev.muons);
  ev.jets = ConstDataVector<xAOD::JetContainer>(SG::VIEW_ELEMENTS);
  for (auto jet : *qa.jets()) {
    if (jet->auxdata<ana::SelectType>("ana_select")) {
      ev.jets.push_back( jet );
    }
  }
  sort_particles(ev.jets);

  for (size_t i = 2; i < ev.jets.size(); ++i) {
    const bool forward = (TMath::Abs(ev.jets[i]->eta()) > 2.5);
    const double pt = ev.jets[i]->pt();
    if ( ! forward && pt > ev.other_central_jet_pt) { ev.other_central_jet_pt = pt; } 
    if (   forward && pt > ev.other_forward_jet_pt) { ev.other_forward_jet_pt = pt; } 
  }
  
  ev.met.SetMagPhi( qa.met()->met(), qa.met()->phi() );
  //ev.met2.SetMagPhi( qa.met2()->met(), qa.met2()->phi() ); 
  ev.met_jet_dphi = minDPhi(qa.met()->phi(), ev.jets);
  ev.lepmet = lepMet(ev.electrons, ev.muons, qa.met());
  ev.lepmet_jet_dphi = minDPhi(ev.lepmet.Phi(), ev.jets);   
  ev.z = Zll(ev.electrons, ev.muons);
    
  if (ev.jets.size() >= 2) {
    ev.jj = ev.jets[0]->p4() + ev.jets[1]->p4();
    ev.jj_deta = deltaEta(ev.jets[0]->eta(), ev.jets[1]->eta());
    ev.jj_dphi = deltaPhi(ev.jets[0]->phi(), ev.jets[1]->phi());
  }
    
  if (ev.electrons.size() >= 1 || ev.muons.size() >= 1) {
    ev.met_lep_mt = MT(ev.electrons, ev.muons, qa.met());
  } 

  if (qa.eventinfo()->eventType(xAOD::EventInfo::IS_SIMULATION)) {
    // ev.true_electron_pt = true_electron_pt(xv);
    // ev.true_muon_pt = true_muon_pt(xv);
    // ev.true_tau_pt = true_tau_pt(xv); 
  }

  ev.mhlt = MHLT(ev.electrons, ev.muons, ev.jets);
  ev.mhlt_lep_mt = MT(ev.electrons, ev.muons, ev.mhlt);

  ev.jj_jet_dphi = minDPhi(ev.jj.Phi(), ev.jets);   
  TLorentzVector minusjj;
  minusjj -= ev.jj;
  ev.minusjj_jet_dphi = minDPhi(minusjj.Phi(), ev.jets);   
  ev.mhlt_jet_dphi = minDPhi(ev.mhlt.Phi(), ev.jets);   

  for (const std::string& t : triggers20152016()) {
    ev.triggers[ t ] = passTrigger(t, xv, qa);
  }
  
  return evptr;
}
