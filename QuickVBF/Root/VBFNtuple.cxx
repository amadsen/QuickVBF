#include <QuickVBF/VBFNtuple.h>
#include <QuickVBF/TriggerPeriods.h>
#include <QuickVBF/safeRetrieve.h>
#include <QuickVBF/Var.h>
#include <RootCoreUtils/Assert.h>
#include <xAODTracking/TrackParticlexAODHelpers.h>
#include <TTree.h>

static double d0sig(const xAOD::Muon* muon, const xAOD::EventInfo& ei) {
  //Transverse impact parameter significance corrected by the beam-spot spread
  const xAOD::TrackParticle* trk = muon->primaryTrackParticle(); 
  RCU_ASSERT( trk );
  const double bsx  = ei.beamPosSigmaX();
  const double bsy  = ei.beamPosSigmaY();
  const double bsxy = ei.beamPosSigmaXY();
  return TMath::Abs(xAOD::TrackingHelpers::d0significance(trk, bsx, bsy, bsxy));
}

namespace {
  struct Vars : BranchHandler {

    Variable<Int_t>    runNumber   { this, "runNumber" };
    Variable<Long64_t> eventNumber { this, "eventNumber" };
    Variable<Int_t>    lumiBlock   { this, "lumiBlock" };

    Vector<Float_t> mcEventWeights { this, "mcEventWeights" };
    
    Variable<Float_t> actualInteractionsPerCrossing  { this, "actualInteractionsPerCrossing" };
    Variable<Float_t> averageInteractionsPerCrossing { this, "averageInteractionsPerCrossing" };
    Variable<Int_t>   nV { this, "nV" };

    Variable<Int_t>   elTrig { this, "elTrig" };
    Variable<Int_t>   e2Trig { this, "e2Trig" };
    Variable<Int_t>   muTrig { this, "muTrig" };
    Variable<Int_t>   xeTrig { this, "xeTrig" };
    Variable<Int_t>   triggerPeriod  { this, "triggerPeriod"  };
    Variable<Float_t> weightElTrigSF { this, "weightElTrigSF" };
    Variable<Float_t> weightE2TrigSF { this, "weightE2TrigSF" };
    Variable<Float_t> weightMuTrigSF { this, "weightMuTrigSF" };
    Variable<Float_t> weightXETrigSF { this, "weightXETrigSF" };
    Variable<Float_t> weightReco     { this, "weightReco"     };
    Variable<Float_t> weightPileup   { this, "weightPileup"   };
    Variable<Float_t> weightNJet     { this, "weightNJet"     };
    Variable<Float_t> weightMC       { this, "weightMC"       };
    
    Variable<Int_t> el_n      { this, "el_n"      };
    Vector<Float_t> el_pt     { this, "el_pt"     };
    Vector<Float_t> el_eta    { this, "el_eta"    };
    Vector<Float_t> el_phi    { this, "el_phi"    };
    Vector<Float_t> el_charge { this, "el_charge" };
    
    // Vector<Int_t>   el_LHLoose { this, "el_LHLoose" };
    // Vector<Int_t>   el_LHMedium { this, "el_LHMedium" };
    // Vector<Int_t>   el_LHTight { this, "el_LHTight" };
    
    Variable<Int_t> mu_n      { this, "mu_n"      };
    Vector<Float_t> mu_pt     { this, "mu_pt"     };
    Vector<Float_t> mu_eta    { this, "mu_eta"    };
    Vector<Float_t> mu_phi    { this, "mu_phi"    };
    Vector<Float_t> mu_charge { this, "mu_charge" };
    Vector<Float_t> mu_d0sig  { this, "mu_d0sig"  };
    
    Variable<Int_t> jet_n     { this, "jet_n"     };
    Vector<Float_t> jet_pt    { this, "jet_pt"    };
    Vector<Float_t> jet_eta   { this, "jet_eta"   };
    Vector<Float_t> jet_phi   { this, "jet_phi"   };
    Vector<Float_t> jet_E     { this, "jet_E"     };
    
    Variable<Float_t> met_et  { this, "met_et"    };
    Variable<Float_t> met_phi { this, "met_phi"   };

    Variable<Float_t> met2_et  { this, "met2_et"    };
    Variable<Float_t> met2_phi { this, "met2_phi"   };
    
    Variable<Float_t> lepmet_et  { this, "lepmet_et" };
    Variable<Float_t> lepmet_phi { this, "lepmet_phi" };
    
    Variable<Float_t> met_jet_dphi { this, "met_jet_dphi" };
    Variable<Float_t> lepmet_jet_dphi { this, "lepmet_jet_dphi" };
    
    Variable<Float_t> Zll_pt  { this, "Zll_pt"  };
    Variable<Float_t> Zll_eta { this, "Zll_eta" };
    Variable<Float_t> Zll_phi { this, "Zll_phi" };
    Variable<Float_t> Zll_m   { this, "Zll_m"   };
    
    Variable<Float_t> jj_pt   { this, "jj_pt"   };
    Variable<Float_t> jj_eta  { this, "jj_eta"  };
    Variable<Float_t> jj_phi  { this, "jj_phi"  };
    Variable<Float_t> jj_m    { this, "jj_m"    };
    Variable<Float_t> jj_deta { this, "jj_deta" };
    Variable<Float_t> jj_dphi { this, "jj_dphi" };

    Variable<Float_t> other_central_jet_pt { this, "other_central_jet_pt" };
    Variable<Float_t> other_forward_jet_pt { this, "other_forward_jet_pt" };
    
    Variable<Float_t> met_lep_mt { this, "met_lep_mt" };

    Variable<Float_t> mhlt_et          { this, "mhlt_et"        };
    Variable<Float_t> mhlt_phi         { this, "mhlt_phi"       };
    Variable<Float_t> mhlt_lep_mt      { this, "mhlt_lep_mt"    };
    Variable<Float_t> jj_jet_dphi      { this, "jj_jet_dphi"      };
    Variable<Float_t> minusjj_jet_dphi { this, "minusjj_jet_dphi" };
    Variable<Float_t> mhlt_jet_dphi    { this, "mhlt_jet_dphi"    };

    Variable<Int_t> HLT_e24_lhmedium_L1EM20VH { this, "HLT_e24_lhmedium_L1EM20VH" };
    Variable<Int_t> HLT_e60_lhmedium { this, "HLT_e60_lhmedium" };
    Variable<Int_t> HLT_e120_lhloose { this, "HLT_e120_lhloose"  };
    Variable<Int_t> HLT_e24_lhtight_nod0_ivarloose { this, "HLT_e24_lhtight_nod0_ivarloose" };
    Variable<Int_t> HLT_e60_lhmedium_nod0 { this, "HLT_e60_lhmedium_nod0" };
    Variable<Int_t> HLT_e140_lhloose_nod0 { this, "HLT_e140_lhloose_nod0" };
    Variable<Int_t> HLT_2e12_lhloose_L1EM10VH { this, "HLT_2e12_lhloose_L1EM10VH" };
    Variable<Int_t> HLT_2e17_lhvloose_nod0 { this, "HLT_2e17_lhvloose_nod0" };
    Variable<Int_t> HLT_mu20_iloose_L1MU15_OR_HLT_mu50 { this, "HLT_mu20_iloose_L1MU15_OR_HLT_mu50" };
    Variable<Int_t> HLT_mu26_ivarmedium_OR_HLT_mu50 { this, "HLT_mu26_ivarmedium_OR_HLT_mu50" };
    Variable<Int_t> HLT_xe70_mht { this, "HLT_xe70_mht" };
    Variable<Int_t> HLT_xe90_mht_L1XE50 { this, "HLT_xe90_mht_L1XE50"  };
    Variable<Int_t> HLT_xe110_mht_L1XE50 { this, "HLT_xe110_mht_L1XE50" };

  };
}


struct VBFNtuple::Impl {
  Vars vars;
  TTree* tree = nullptr;
};


VBFNtuple::VBFNtuple() : p_(new VBFNtuple::Impl) {}

VBFNtuple::~VBFNtuple() {}

void VBFNtuple::setTree(TTree* tree) {
  p_->tree = tree;
  if (p_->tree) {
    connect(p_->vars, p_->tree);
  }
}

void VBFNtuple::fill(const WB& wb) {
  RCU_REQUIRE(wb.vbfEvent);

  clear(p_->vars);

  const auto& ei = *safeRetrieve<xAOD::EventInfo>(wb.xAODEvent, "EventInfo"); 
  const VBFEvent& ev = *wb.vbfEvent.get();
  auto& out = p_->vars;

  unsigned runNumber = 0;
  if (wb.qa->eventinfo()->eventType(xAOD::EventInfo::IS_SIMULATION)) {
    runNumber = ei.auxdata<unsigned int>("RandomRunNumber");
  } else {
    runNumber = ei.runNumber();
  }
  
  out.elTrig(wb.vbfWeights->elTrig);
  out.e2Trig(wb.vbfWeights->e2Trig);
  out.muTrig(wb.vbfWeights->muTrig);
  out.xeTrig(wb.vbfWeights->xeTrig);
  out.triggerPeriod(triggerPeriodCategory(runNumber));
  out.weightElTrigSF(wb.vbfWeights->weightElTrigSF);
  out.weightE2TrigSF(wb.vbfWeights->weightE2TrigSF);
  out.weightMuTrigSF(wb.vbfWeights->weightMuTrigSF);
  out.weightXETrigSF(wb.vbfWeights->weightXETrigSF);
  out.weightReco(wb.vbfWeights->weightReco);
  out.weightPileup(wb.vbfWeights->weightPileup);
  out.weightNJet(wb.vbfWeights->weightNJet);
  out.weightMC(wb.vbfWeights->weightMC);
  
  out.runNumber(      runNumber     );
  out.eventNumber( ei.eventNumber() );
  out.lumiBlock(   ei.lumiBlock()   );
  out.actualInteractionsPerCrossing( ei.actualInteractionsPerCrossing() );
  out.averageInteractionsPerCrossing( ei.averageInteractionsPerCrossing() );
  out.nV( wb.nV );

  if (wb.qa->eventinfo()->eventType(xAOD::EventInfo::IS_SIMULATION)) {
    if (std::string(wb.systematic) == "nominal") {
      for (Float_t w : ei.mcEventWeights()) {
	out.mcEventWeights().push_back( w );
      }
    }
  }
  
  for (auto electron : ev.electrons) {
    out.el_pt(     ).push_back( electron->pt()     );
    out.el_eta(    ).push_back( electron->eta()    );
    out.el_phi(    ).push_back( electron->phi()    );
    out.el_charge( ).push_back( electron->charge() );
    // out.el_LHLoose().push_back( electron->auxdata<ana::SelectType>("QuickAna_LooseAndBLayerLLH") );
    // out.el_LHLoose().push_back( electron->auxdata<ana::SelectType>("QuickAna_MediumLLH") );
    // out.el_LHLoose().push_back( electron->auxdata<ana::SelectType>("QuickAna_TightLLH") );
  }
  out.el_n( ev.electrons.size() );
  for (auto muon : ev.muons) {
    out.mu_pt(    ).push_back( muon->pt()      );
    out.mu_eta(   ).push_back( muon->eta()     );
    out.mu_phi(   ).push_back( muon->phi()     );
    out.mu_charge().push_back( muon->charge()  );
    out.mu_d0sig( ).push_back( d0sig(muon, ei) );
  }
  out.mu_n( ev.muons.size() );
  for (auto jet : ev.jets) {
    out.jet_pt( ).push_back( jet->pt()  );
    out.jet_eta().push_back( jet->eta() );
    out.jet_phi().push_back( jet->phi() );
    out.jet_E(  ).push_back( jet->e()   );
  }
  out.jet_n( ev.jets.size() );
  out.other_central_jet_pt( ev.other_central_jet_pt );
  out.other_forward_jet_pt( ev.other_forward_jet_pt ); 
  out.met_et(          ev.met.Mod()       );
  out.met_phi(         ev.met.Phi()       );
  out.met2_et(          ev.met2.Mod()       );
  out.met2_phi(         ev.met2.Phi()       );
  out.met_jet_dphi(    ev.met_jet_dphi    );
  out.lepmet_et(       ev.lepmet.Mod()    );
  out.lepmet_phi(      ev.lepmet.Phi()    );
  out.lepmet_jet_dphi( ev.lepmet_jet_dphi );
  out.Zll_pt(          ev.z.Pt()          );
  out.Zll_eta(         ev.z.Eta()         );
  out.Zll_phi(         ev.z.Phi()         );
  out.Zll_m(           ev.z.M()           );
  out.jj_pt(           ev.jj.Pt()         );
  out.jj_eta(          ev.jj.Eta()        );
  out.jj_phi(          ev.jj.Phi()        );
  out.jj_m(            ev.jj.M()          );
  out.jj_deta(         ev.jj_deta         );
  out.jj_dphi(         ev.jj_dphi         );
  out.met_lep_mt(      ev.met_lep_mt      );
  
  out.mhlt_et(          ev.mhlt.Mod()       );
  out.mhlt_phi(         ev.mhlt.Phi()       );
  out.mhlt_lep_mt(      ev.mhlt_lep_mt      );
  out.jj_jet_dphi(      ev.jj_jet_dphi      );
  out.minusjj_jet_dphi( ev.minusjj_jet_dphi );
  out.mhlt_jet_dphi(    ev.mhlt_jet_dphi    );

  out.HLT_e24_lhmedium_L1EM20VH( ev.triggers.at("HLT_e24_lhmedium_L1EM20VH") );
  out.HLT_e60_lhmedium( ev.triggers.at("HLT_e60_lhmedium") );
  out.HLT_e120_lhloose( ev.triggers.at("HLT_e120_lhloose") );
  out.HLT_e24_lhtight_nod0_ivarloose( ev.triggers.at("HLT_e24_lhtight_nod0_ivarloose") );
  out.HLT_e60_lhmedium_nod0( ev.triggers.at("HLT_e60_lhmedium_nod0") );
  out.HLT_e140_lhloose_nod0( ev.triggers.at("HLT_e140_lhloose_nod0") );
  out.HLT_2e12_lhloose_L1EM10VH( ev.triggers.at("HLT_2e12_lhloose_L1EM10VH") );
  out.HLT_2e17_lhvloose_nod0( ev.triggers.at("HLT_2e17_lhvloose_nod0") );
  out.HLT_mu20_iloose_L1MU15_OR_HLT_mu50( ev.triggers.at("HLT_mu20_iloose_L1MU15_OR_HLT_mu50") );
  out.HLT_mu26_ivarmedium_OR_HLT_mu50( ev.triggers.at("HLT_mu26_ivarmedium_OR_HLT_mu50") );
  out.HLT_xe70_mht( ev.triggers.at("HLT_xe70_mht") );
  out.HLT_xe90_mht_L1XE50( ev.triggers.at("HLT_xe90_mht_L1XE50") );
  out.HLT_xe110_mht_L1XE50( ev.triggers.at("HLT_xe110_mht_L1XE50") );
  
  if (p_->tree) {
    p_->tree->Fill();
  }
}
