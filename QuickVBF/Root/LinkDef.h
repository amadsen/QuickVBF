#include <QuickVBF/QuickVBF.h>
#include <QuickVBF/QVBFConfig.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#endif

#ifdef __CINT__
#pragma link C++ class QuickVBF+;
#pragma link C++ class QVBFConfig+;
#endif
