#include <QuickVBF/HistVar.h>
#include <RootCoreUtils/Assert.h>

static std::vector<Double_t> mkbins(const unsigned n, const Double_t lo, const Double_t hi) {
  RCU_REQUIRE(n >= 1);
  RCU_REQUIRE(lo < hi);
  std::vector<Double_t> v;
  for (unsigned i = 0; i <= n; i++) {
    v.push_back(lo + i * (hi - lo) / static_cast<double>( n ));
  }
  return v;
}

HistVar::HistVar(const std::string name,
		 const std::string title,
		 const std::vector<Double_t> bins,
		 const Unit unit,
		 const std::function<double(const WB&)> calc)
  : name(name)
  , title(title)
  , bins(bins)
  , unit(unit)
  , calc(calc)
{
  RCU_REQUIRE(not name.empty());
  RCU_REQUIRE(not title.empty());
  RCU_REQUIRE(bins.size());
  RCU_REQUIRE(unit.value != 0);
}
    
HistVar::HistVar(const std::string name,
		 const std::string title,
		 const unsigned n, const Double_t min, const Double_t max,
		 const Unit unit,
		 const std::function<double(const WB&)> calc)
  : HistVar(name, title, mkbins(n, min, max), unit, calc) {}

HistVar::HistVar(const std::string name,
		 const std::string title,
		 const Double_t min, const Double_t max,
		 const Unit unit,
		 const std::function<double(const WB&)> calc)
  : HistVar(name, title, max-min, min, max, unit, calc) {}
