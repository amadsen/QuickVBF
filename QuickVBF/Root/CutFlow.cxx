
#include <QuickVBF/CutFlow.h>
#include <QuickVBF/HistStore.h>
#include <QuickVBF/WB.h>
#include <RootCoreUtils/Assert.h>
#include <TH1D.h>

static std::string name(const SysID sys, const FlowID flow, const VarID var) {
    RCU_REQUIRE(not std::string(sys).empty());
    RCU_REQUIRE(not std::string(flow).empty());
    RCU_REQUIRE(not std::string(var).empty());  
    return std::string(sys) + "/" + std::string(flow) + "/" + std::string(var);
  }

static std::string title(const HistVar& v) { 
   return v.name + ";" +
          v.title + (v.unit.label.empty() ? "" : " [" + v.unit.label + "]") + ";" +
          "Events";
}

static VarID varID(const HistVar& var) {
  return VarID(var.name);
}

void prepare(const SysID& sysID, const Flow& flow, HistStore& histStore) {

  if (not flow.systematics && std::string(sysID) != "nominal") { return; }
  if (flow.hists.size() == 0 && not flow.writeNtuple) { return; }
    
  FlowID flowID(flow.id);
  for (const HistVar& v : flow.hists) {
    VarID varID(v.name);
    
    std::unique_ptr<TH1D> hist(new TH1D(name(sysID, flowID, varID).c_str(), title(v).c_str(),
					v.bins.size()-1, &v.bins[0])); 
    hist->SetDirectory(nullptr);
    hist->Sumw2();
    histStore.set(sysID, flowID, varID, hist.release());
  }

  VarID varID("CUTFLOW");
  std::unique_ptr<TH1D> cf(new TH1D(name(sysID, flowID, varID).c_str(), "CUTFLOW",
				    flow.items.size(), 0, flow.items.size())); 
  for (int b = 1; b <= cf->GetNbinsX(); ++b) {
    cf->GetXaxis()->SetBinLabel(b, flow.items[b-1]->label().c_str());
  }
  histStore.set(sysID, flowID, varID, cf.release());  
}

bool process(const SysID& sysID, const Flow& flow, const WB& wb, HistStore& histStore) {

  if (not flow.systematics && std::string(sysID) != "nominal") { return false; }
  if (flow.hists.size() == 0 && not flow.writeNtuple) { return false; }
  
  double w = 1.;
  for (size_t i = 0; i != flow.items.size(); ++i) {
    w *= flow.items.at( i )->apply( wb );  
    if (w == 0) { return false; }
    histStore.get(sysID, FlowID(flow.id), VarID("CUTFLOW"))->Fill( i, w );
  }
  for (const HistVar& v : flow.hists) {
    histStore.get(sysID, FlowID(flow.id), varID(v))->Fill( v.calc(wb) / v.unit.value, w );
  }
  return true;
}
