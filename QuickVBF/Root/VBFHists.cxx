#include <QuickVBF/VBFHists.h>
#include <QuickVBF/utils.h>
#include <QuickVBF/WB.h>
#include <RootCoreUtils/Assert.h>

#define CALC(X)  [](const WB& wb)->double{ return X; }

namespace {
  static const Unit GeV {"GeV", 1000.};
  static const Unit rad {"", 1.};
  static const Unit num {"", 1.};
}

std::vector<HistVar> VBFHists::nV() {
  std::vector<HistVar> v;
  v.emplace_back("nV",  "nV",  0, 30, num, CALC(wb.nV));
  return v;
}

std::vector<HistVar> VBFHists::Zee() {
  std::vector<HistVar> v;
  v.emplace_back("Zll_pt",    "Zll_pt",    0,  500, GeV, CALC(wb.vbfEvent->z.Pt()));
  v.emplace_back("Zll_m",     "Zll_m",     0,  200, GeV, CALC(wb.vbfEvent->z.M()));
  v.emplace_back("e1_pt",     "e1_pt",     0,  300, GeV, CALC((wb.vbfEvent->electrons.size() >= 1 ? wb.vbfEvent->electrons[0]->pt() : 0)));
  v.emplace_back("e2_pt",     "e2_pt",     0,  300, GeV, CALC((wb.vbfEvent->electrons.size() >= 2 ? wb.vbfEvent->electrons[1]->pt() : 0)));
  v.emplace_back("met_et",    "met_et",    0, 1000, GeV, CALC(wb.vbfEvent->met.Mod()));
//  v.emplace_back("lepmet_et", "lepmet_et", 0,  500, GeV, CALC(wb.vbfEvent->lepmet.Mod()));
  v.emplace_back("jet_n",     "NJET",      0,   30, num, CALC(wb.vbfEvent->jets.size()));  
  v.emplace_back("j1_pt",     "j1_pt",     0,  300, GeV, CALC((wb.vbfEvent->jets.size() >= 1 ? wb.vbfEvent->jets[0]->pt()  : 0)));
  v.emplace_back("j2_pt",     "j2_pt",     0,  300, GeV, CALC((wb.vbfEvent->jets.size() >= 2 ? wb.vbfEvent->jets[1]->pt()  : 0)));
  v.emplace_back("nV",  "nV",  0, 30, num, CALC(wb.nV));
  return v;
}

std::vector<HistVar> VBFHists::Zmm() {
  std::vector<HistVar> v;
  v.emplace_back("Zll_pt",    "Zll_pt",    0,  500, GeV, CALC(wb.vbfEvent->z.Pt()));
  v.emplace_back("Zll_m",     "Zll_m",     0,  200, GeV, CALC(wb.vbfEvent->z.M()));
  v.emplace_back("m1_pt",     "m1_pt",     0,  300, GeV, CALC((wb.vbfEvent->muons.size() >= 1 ? wb.vbfEvent->muons[0]->pt()  : 0)));
  v.emplace_back("m2_pt",     "m2_pt",     0,  300, GeV, CALC((wb.vbfEvent->muons.size() >= 2 ? wb.vbfEvent->muons[1]->pt()  : 0)));
  v.emplace_back("met_et",    "met_et",    0, 1000, GeV, CALC(wb.vbfEvent->met.Mod()));
//  v.emplace_back("lepmet_et", "lepmet_et", 0,  500, GeV, CALC(wb.vbfEvent->lepmet.Mod()));
  v.emplace_back("jet_n",     "NJET",      0,   30, num, CALC(wb.vbfEvent->jets.size()));  
  v.emplace_back("j1_pt",     "j1_pt",     0,  300, GeV, CALC((wb.vbfEvent->jets.size() >= 1 ? wb.vbfEvent->jets[0]->pt()  : 0)));
  v.emplace_back("j2_pt",     "j2_pt",     0,  300, GeV, CALC((wb.vbfEvent->jets.size() >= 2 ? wb.vbfEvent->jets[1]->pt()  : 0)));
  v.emplace_back("nV",  "nV",  0, 30, num, CALC(wb.nV));  
  return v;
}

std::vector<HistVar> VBFHists::Wev() {
  std::vector<HistVar> v;
  v.emplace_back("e1_pt",      "e1_pt",       0,  300, GeV, CALC((wb.vbfEvent->electrons.size() >= 1 ? wb.vbfEvent->electrons[0]->pt() : 0)));
  v.emplace_back("e1_eta",     "e1_eta", 40, -3,    3, rad, CALC((wb.vbfEvent->electrons.size() >= 1 ? wb.vbfEvent->electrons[0]->eta() : -99)));
  v.emplace_back("met_et",     "met_et",      0, 1000, GeV, CALC(wb.vbfEvent->met.Mod()));
//  v.emplace_back("lepmet_et",  "lepmet_et",   0,  500, GeV, CALC(wb.vbfEvent->lepmet.Mod()));
  v.emplace_back("met_lep_mt", "met_lep_mt",  0,  500, GeV, CALC(wb.vbfEvent->met_lep_mt));  
  v.emplace_back("jet_n",      "NJET",        0,   30, num, CALC(wb.vbfEvent->jets.size()));  
//  v.emplace_back("j1_pt",      "j1_pt",       0,  300, GeV, CALC((wb.vbfEvent->jets.size() >= 1 ? wb.vbfEvent->jets[0]->pt()  : 0)));
//  v.emplace_back("j2_pt",      "j2_pt",       0,  300, GeV, CALC((wb.vbfEvent->jets.size() >= 2 ? wb.vbfEvent->jets[1]->pt()  : 0)));
  return v;
}

std::vector<HistVar> VBFHists::Wmv() {
  std::vector<HistVar> v;
  v.emplace_back("m1_pt",      "m1_pt",       0,  300, GeV, CALC((wb.vbfEvent->muons.size() >= 1 ? wb.vbfEvent->muons[0]->pt()  : 0)));
  v.emplace_back("m1_eta",     "m1_eta", 40, -3,    3, rad, CALC((wb.vbfEvent->muons.size() >= 1 ? wb.vbfEvent->muons[0]->eta() : -99)));
  v.emplace_back("met_et",     "met_et",      0, 1000, GeV, CALC(wb.vbfEvent->met.Mod()));
//  v.emplace_back("lepmet_et",  "lepmet_et",   0,  500, GeV, CALC(wb.vbfEvent->lepmet.Mod()));
  v.emplace_back("met_lep_mt", "met_lep_mt",  0,  500, GeV, CALC(wb.vbfEvent->met_lep_mt));  
  v.emplace_back("jet_n",      "NJET",        0,   30, num, CALC(wb.vbfEvent->jets.size()));  
//  v.emplace_back("j1_pt",      "j1_pt",       0,  300, GeV, CALC((wb.vbfEvent->jets.size() >= 1 ? wb.vbfEvent->jets[0]->pt()  : 0)));
//  v.emplace_back("j2_pt",      "j2_pt",       0,  300, GeV, CALC((wb.vbfEvent->jets.size() >= 2 ? wb.vbfEvent->jets[1]->pt()  : 0)));
  return v;
}

std::vector<HistVar> VBFHists::JJ() {
  std::vector<HistVar> v;
  v.emplace_back("jj_pt",           "jj_pt",               0,  500, GeV, CALC(wb.vbfEvent->jj.Pt()));
  v.emplace_back("jj_m",            "jj_m",                0, 4000, GeV, CALC(wb.vbfEvent->jj.M()));
  v.emplace_back("jj_deta",         "jj_deta",         40, 0,   10, rad, CALC(wb.vbfEvent->jj_deta));
  v.emplace_back("jj_dphi",         "jj_dphi",         40, 0,    4, rad, CALC(wb.vbfEvent->jj_dphi));
  // v.emplace_back("lepmet_et",       "lepmet_et",           0,  500, GeV, CALC(wb.vbfEvent->lepmet.Mod()));
  // v.emplace_back("lepmet_jet_dphi", "lepmet_jet_dphi", 40, 0,    4, rad, CALC(wb.vbfEvent->lepmet_jet_dphi));
  return v;
}

std::vector<HistVar> VBFHists::nEvt() {
  std::vector<HistVar> v;
  v.emplace_back("nEvt",  "nEvt",  0, 1, num, CALC( 0 ));
  // v.emplace_back("truth_el_pt",  "truth_el_pt",  0,  300, GeV, CALC(wb.vbfEvent->true_electron_pt));
  // v.emplace_back("truth_mu_pt",  "truth_mu_pt",  0,  300, GeV, CALC(wb.vbfEvent->true_muon_pt));
  // v.emplace_back("truth_tau_pt", "truth_tau_pt", 0,  300, GeV, CALC(wb.vbfEvent->true_tau_pt));
  return v;
}

std::vector<HistVar> VBFHists::allHists() {
  std::vector<HistVar> v;
  astd::move_items(nV(),   v);
  astd::move_items(Zee(),  v);
  astd::move_items(Zmm(),  v);
  astd::move_items(Wev(),  v);
  astd::move_items(Wmv(),  v);
  astd::move_items(JJ(),   v);
  astd::move_items(nEvt(), v);
  return v;
}

