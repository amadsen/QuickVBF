#include <QuickVBF/Var.h>

void clear(BranchHandler& bh) {
  for (auto p : bh.branches) {
    p->clear();
  }
}

void connect(const BranchHandler& bh, TTree* tree) {
  for (auto p : bh.branches) {
    p->connect(tree);
  }
}
