#include <QuickVBF/NtupleWriter.h>
#include <QuickVBF/VBFNtuple.h>
#include <RootCoreUtils/Assert.h>
#include <TTree.h>
#include <limits>

struct NtupleWriter::Impl {
  std::unique_ptr<VBFNtuple> vars;
};

NtupleWriter::NtupleWriter(VBFNtuple* content)
  : p_(new NtupleWriter::Impl) {
  RCU_REQUIRE(content);
  p_->vars.reset(content);
}

NtupleWriter::~NtupleWriter() {}

void NtupleWriter::initialize(TTree* tree) {
  p_->vars->setTree(tree); 
  tree->SetAutoSave(std::numeric_limits<Long64_t>::max());
}

void NtupleWriter::process(const WB& wb) {
  p_->vars->fill(wb);
}
