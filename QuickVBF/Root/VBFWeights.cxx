#include <QuickVBF/VBFWeights.h>
#include <QuickVBF/safeRetrieve.h>
#include <QuickVBF/TriggerPeriods.h>
#include <QuickVBF/VBFEvent.h>
#include <RootCoreUtils/Assert.h>
#include <PMGTools/PMGSherpa22VJetsWeightTool.h>
#include <QuickAna/QuickAna.h>
#include <cmath>

static unsigned getDataMCRunNumber(xAOD::TEvent& xv, ana::IQuickAna& qa) {
  unsigned runNumber = 0;
  if (qa.eventinfo()->eventType(xAOD::EventInfo::IS_SIMULATION)) {
    runNumber = safeRetrieve<xAOD::EventInfo>(&xv, "EventInfo")->auxdata<unsigned int>("RandomRunNumber");
    if (runNumber == 0) {
      //This can happen, see comments in QuickAna TriggerTool  
      runNumber = 284285; //Max lumi run in 2015 <- update this to 2016
    }
  } else {
    runNumber = safeRetrieve<xAOD::EventInfo>(&xv, "EventInfo")->runNumber();
  }
  return runNumber;
}

static bool passAnyTrigger(const std::vector<std::string>& triggers, const VBFEvent& vbfEvent) {
  for (const auto& t : triggers) {
    if (vbfEvent.triggers.at(t)) { return true; }
  }
  return false;
} 

static double weightMuTrigSF(xAOD::TEvent& xv, ana::IQuickAna& qa, const VBFEvent& vbfEvent) {
  if (not qa.eventinfo()->eventType(xAOD::EventInfo::IS_SIMULATION)) { return 1.; }
  if (vbfEvent.muons.size() == 0) { return 1.; }
  unsigned runNumber = getDataMCRunNumber(xv, qa);
  const auto triggers = muTrigger(runNumber);
  for (const std::string& trigger : triggers) {
    if (qa.eventinfo()->auxdata<bool>(trigger+"_passTrig")) {
      return qa.eventinfo()->auxdata<double>(trigger+"_Mu_TrigSF");
    }
  }
  return 1.0;
}

static double weightElTrigSF(xAOD::TEvent& xv, ana::IQuickAna& qa, const VBFEvent& vbfEvent) {
  if (not qa.eventinfo()->eventType(xAOD::EventInfo::IS_SIMULATION)) { return 1.; }
  if (vbfEvent.electrons.size() == 0) { return 1.; } 
  unsigned runNumber = getDataMCRunNumber(xv, qa);
  const auto triggers = elTrigger(runNumber);
  for (const std::string& trigger : triggers) {
    //Egamma provides SF for combinations of multiple triggers
    //If any of these triggers are used, QuickAna returns the combined SF
    //so we check all triggers but apply the sf only once
    if (qa.eventinfo()->auxdata<bool>(trigger+"_passTrig")) {
      for (size_t i = 0; i != vbfEvent.electrons.size(); ++i) {
//	if (vbfEvent.electrons.at(i)->auxdata<bool>(trigger+"_trigMatch") ) {
	  return vbfEvent.electrons.at(i)->auxdata<double>(trigger+"_TrigSF");
//	}
      }
    }
  }
  return 1.0;      
}

static double weightXETrigSF(xAOD::TEvent&, ana::IQuickAna&, const VBFEvent& vbfEvent) {
  static const double p0 = 59.3407;
  static const double p1 = 54.9134;
  static const double GeV = 1000.;
  double x = vbfEvent.jj.Pt() / GeV;
  //RCU_ASSERT(x >= 100);
  if (x < 100) { return 0; }
  if (x > 240) { x = 240; }
  const double sf = 0.5*(1+TMath::Erf((x-p0)/(TMath::Sqrt(2)*p1)));
  RCU_PROVIDE(sf > 0);
  RCU_PROVIDE(sf <= 1.5);
  return sf;
}  

static double weightE2TrigSF(xAOD::TEvent& xv, ana::IQuickAna& qa, const VBFEvent& vbfEvent) {
  if (not qa.eventinfo()->eventType(xAOD::EventInfo::IS_SIMULATION)) { return 1.; }
  if (vbfEvent.electrons.size() == 0) { return 1.; } 
  unsigned runNumber = getDataMCRunNumber(xv, qa);
  const auto triggers = e2Trigger(runNumber);
  RCU_ASSERT(triggers.size() == 1); //Expect only one e2 trigger per period
  const std::string& trigger = triggers.at(0);
  if (qa.eventinfo()->auxdata<bool>(trigger+"_passTrig")) {
    double sf = 1.;
    for (size_t i = 0; i != vbfEvent.electrons.size(); ++i) {
//      if (vbfEvent.electrons.at(i)->auxdata<bool>(trigger+"_trigMatch") ) {	
	sf *= vbfEvent.electrons.at(i)->auxdata<double>(trigger+"_TrigSF");
//      }
    }
    return sf;
  }
  return 1.0;      
}

static double weightPileup(ana::IQuickAna& qa) {
  if (not qa.eventinfo()->eventType(xAOD::EventInfo::IS_SIMULATION)) { return 1.; }
  return qa.eventinfo()->auxdata<float>("PileupWeight");
}

static double weightMC(ana::IQuickAna& qa, xAOD::TEvent& xv) {
  if (not qa.eventinfo()->eventType(xAOD::EventInfo::IS_SIMULATION)) { return 1.; }
  auto const * const ei = safeRetrieve<xAOD::EventInfo>(&xv, "EventInfo");
  double w = (ei->mcEventWeights().size() ? ei->mcEventWeights().at(0) : 1.); 
  const unsigned dsid = ei->mcChannelNumber();
  RCU_REQUIRE(dsid > 100000 && dsid < 999999);
  if ((dsid >= 363100 && dsid <= 363500) ||
      (dsid >= 364100 && dsid <= 364197) ||
      (dsid >= 304015 && dsid <= 304021)) { //Sherpa
    if (w < -100. || w > 100.) {
      w = 1;
    }
  }
  return w;
}

namespace { using PMG = PMGTools::PMGSherpa22VJetsWeightTool; }

static std::unique_ptr<PMG> initPMG() {
  std::unique_ptr<PMG> pmg{new PMG("QuickVBF_WeightNJet_PMGSherpa22VJetsWeightTool")};
  RCU_ASSERT(pmg->setProperty("TruthJetContainer", "AntiKt4TruthJets").isSuccess());
  RCU_ASSERT(pmg->initialize().isSuccess());
  return std::move( pmg );
}    

double weightNJet(ana::IQuickAna& qa, xAOD::TEvent& xv) {
  if (not qa.eventinfo()->eventType(xAOD::EventInfo::IS_SIMULATION)) { return 1.; }
  auto const * const ei = safeRetrieve<xAOD::EventInfo>(&xv, "EventInfo");
  unsigned dsid = ei->mcChannelNumber();
  RCU_REQUIRE(dsid > 100000 && dsid < 999999);
  if (dsid < 363100 || dsid > 363500) { return 1.; }
  static std::unique_ptr<PMG> pmg = initPMG();
  return pmg->getWeight();
}

std::shared_ptr<VBFWeights> calcWeights(xAOD::TEvent& xv, ana::IQuickAna& qa,
					const VBFEvent& vbfEvent, const SysID sysID) {  

  const unsigned runNumber = getDataMCRunNumber(xv, qa);

  std::shared_ptr<VBFWeights> wptr(new VBFWeights);
  VBFWeights& w = *wptr.get();
  w.elTrig         = passAnyTrigger(elTrigger(runNumber), vbfEvent);
  w.e2Trig         = passAnyTrigger(e2Trigger(runNumber), vbfEvent);
  w.muTrig         = passAnyTrigger(muTrigger(runNumber), vbfEvent);
  w.xeTrig         = passAnyTrigger(metTrigger(runNumber), vbfEvent);
  w.weightMuTrigSF = weightMuTrigSF(xv, qa, vbfEvent);
  w.weightElTrigSF = weightElTrigSF(xv, qa, vbfEvent);
  w.weightE2TrigSF = weightE2TrigSF(xv, qa, vbfEvent);
  w.weightXETrigSF = weightXETrigSF(xv, qa, vbfEvent);
  w.weightReco     = qa.weight();
  w.weightPileup   = weightPileup(qa);
  w.weightNJet     = weightNJet(qa, xv); 
  w.weightMC       = weightMC(qa, xv);

  if (std::isnan(w.weightPileup)) { w.weightPileup = 1.; } //It happens some times

  const std::string sys = static_cast<std::string>(sysID);
  if (sys == "NO_WEIGHTS" || sys == "NO_WEIGHT_TRIGGER") { w.weightElTrigSF = 1.; }
  if (sys == "NO_WEIGHTS" || sys == "NO_WEIGHT_TRIGGER") { w.weightE2TrigSF = 1.; }
  if (sys == "NO_WEIGHTS" || sys == "NO_WEIGHT_TRIGGER") { w.weightMuTrigSF = 1.; }
  if (sys == "NO_WEIGHTS" || sys == "NO_WEIGHT_TRIGGER") { w.weightXETrigSF = 1.; }
  if (sys == "NO_WEIGHTS" || sys == "NO_WEIGHT_RECO")    { w.weightReco     = 1.; }
  if (sys == "NO_WEIGHTS" || sys == "NO_WEIGHT_PILEUP")  { w.weightPileup   = 1.; }
  if (sys == "NO_WEIGHTS" || sys == "NO_WEIGHT_NJET")    { w.weightNJet     = 1.; }
  if (sys == "NO_WEIGHTS" || sys == "NO_WEIGHT_MC")      { w.weightMC       = 1.; }
  return wptr;
}
