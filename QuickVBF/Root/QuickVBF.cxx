#include <QuickVBF/QuickVBF.h>
#include <QuickVBF/CutGRL.h>
#include <QuickVBF/global.h>
#include <QuickVBF/HistStore.h>
#include <QuickVBF/safeRetrieve.h>
#include <QuickVBF/utils.h>
#include <QuickVBF/VBFCuts.h>
#include <QuickVBF/VBFEvent.h>
#include <QuickVBF/VBFHists.h>
#include <QuickVBF/VBFNtuple.h>
#include <QuickVBF/NtupleWriter.h>
#include <QuickVBF/VBFWeights.h>
#include <QuickVBF/WB.h>
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/OutputStream.h>
#include <EventLoop/Worker.h>
#include <PATInterfaces/CorrectionCode.h>
#include <PATInterfaces/SystematicsUtil.h>
#include <QuickAna/QuickAna.h>
#include <xAODCore/tools/ReadStats.h>
#include <xAODCore/tools/IOStats.h>
#include <xAODCutFlow/CutBookkeeper.h>
#include <xAODCutFlow/CutBookkeeperContainer.h>
#include <TFile.h>
#include <TH1.h>
#include <TList.h>
#include <TMath.h>
#include <TParameter.h>
#include <TSystem.h>
#include <TSystemDirectory.h>
#include <TSystemFile.h>
#include <TTree.h>
#include <iostream>
#include <string>
#include <vector>
#include <map>

static std::string findMuData(const std::string& dirGRL) {
  return gSystem->ExpandPathName( (dirGRL + "/ilumicalc.root").c_str() );
} 

static std::vector<std::string> findMuMC(std::string dirPRW) {
  dirPRW = gSystem->ExpandPathName(dirPRW.c_str());
  std::vector<std::string> found;
  TSystemDirectory dir(dirPRW.c_str(), dirPRW.c_str());
  TList *files = dir.GetListOfFiles();
  if (files) {
    TSystemFile *file;
    TString fname;
    TIter next(files);
    while ((file=(TSystemFile*)next())) {
      fname = file->GetName();
      if (not file->IsDirectory() && fname.EndsWith(".root")) {
	found.emplace_back(dirPRW + "/" + fname.Data());
      }
    }
  }
  return found;
}

static bool isSampleData(const std::string& sampleName) {
  if (sampleName.find("data15_13TeV") != std::string::npos) { return true; }
  if (sampleName.find("data16_13TeV") != std::string::npos) { return true; }
  return false;
}

static bool isSampleMC(const std::string& sampleName) {
  if (sampleName.find("mc15_13TeV") != std::string::npos) { return true; }
  if (sampleName.find("mc16_13TeV") != std::string::npos) { return true; }
  return false;
}

static SysID systematicID(const CP::SystematicSet& sys) {
  return SysID((sys.name().empty() ? "nominal" : sys.name()));
}

static VarID varID(const HistVar& var) {
  return VarID(var.name);
}

ClassImp(QuickVBF)

struct QuickVBF::Impl {
  bool isData = false;
  bool isMC = false;
  int nEvents = 0;
  double sumEventWeights = 0;
  uint64_t orig_nEventsProcessed = 0;  
  double   orig_sumOfWeights = 0;
  double   orig_sumOfWeightsSquared = 0; 
  std::unique_ptr<ana::IQuickAna> quickAna;
  std::unique_ptr<NtupleWriter> ntupleWriter;
  std::vector<CP::SystematicSet> variations;
  std::vector<Flow> cutflows;
  HistStore histStore;
  CutGRL cutGRL;
  bool initialized = false;

  std::map<SysID, std::unique_ptr<NtupleWriter>> ntupleWriters;
};

void QuickVBF::testInvariant() const {
  RCU_INVARIANT(this != nullptr);
  RCU_INVARIANT(p_.get() != nullptr);
  RCU_INVARIANT(p_->initialized != (p_->isData == p_->isMC));
}

QuickVBF::QuickVBF(const QVBFConfig cfg)
  : cfg(cfg), p_{astd::make_unique<Impl>()} {
  RCU_NEW_INVARIANT(this);
}

QuickVBF::~QuickVBF() {
  RCU_DESTROY_INVARIANT(this);
}

EL::StatusCode QuickVBF::setupJob(EL::Job& job) {
  RCU_CHANGE_INVARIANT(this);
  job.useXAOD();
  if (this->cfg.writeNtupleNominal) {
    job.outputAdd(EL::OutputStream("nominal"));
  }
  if (this->cfg.writeNtupleSystematics) {
    job.outputAdd(EL::OutputStream("systematics"));
  }
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode QuickVBF::histInitialize() {
  RCU_CHANGE_INVARIANT(this);
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode QuickVBF::fileExecute() {
  RCU_CHANGE_INVARIANT(this);

  xAOD::TEvent* event = wk()->xaodEvent();
  TTree *metaData = dynamic_cast<TTree*>(wk()->inputFile()->Get("MetaData"));
  if (not metaData) {
    Error("fileExecute()", "MetaData not found! Exiting.");
    return EL::StatusCode::FAILURE;
  }
  metaData->LoadTree(0);
  const bool isDerivation = not metaData->GetBranch("StreamAOD");

  if (isDerivation) {  
    const xAOD::CutBookkeeperContainer* completeCBC = 0;
    if (not event->retrieveMetaInput(completeCBC, "CutBookkeepers").isSuccess()) {
      Error("QuickVBF", "Failed to retrieve CutBookkeepers");
      return EL::StatusCode::FAILURE;
    }
    std::string streamDAOD;
    const xAOD::CutBookkeeper* allEventsCBK = nullptr;
    int maxCycle = -1;
    for (auto cbk : *completeCBC) {
      if (false) {
	std::cout << cbk->nameIdentifier() << " : " << cbk->name()
		  << " : desc = "          << cbk->description()
		  << " : inputStream = "   << cbk->inputStream()
		  << " : outputStreams = " << (cbk->outputStreams().size() ? cbk->outputStreams()[0] : "")
		  << " : cycle = "         << cbk->cycle()
		  << " : allEvents = "     << cbk->nAcceptedEvents()
		<< std::endl;
      }
      if (cbk->name() == "AllExecutedEvents" && TString(cbk->inputStream()).Contains("StreamDAOD")) {
	streamDAOD = TString(cbk->inputStream()).ReplaceAll("Stream", "").Data();
      }
      if (cbk->cycle() > maxCycle && cbk->name() == "AllExecutedEvents" && cbk->inputStream() == "StreamAOD") {
	allEventsCBK = cbk;
	maxCycle = cbk->cycle();
      }
    }
    if (allEventsCBK) {
      Info("QuickVBF", "File is %s", streamDAOD.c_str());
      Info("QuickVBF", "Retrieved CutBookkeepers: accepted %lu SumW %f SumW2 %f ",
	   p_->orig_nEventsProcessed    += allEventsCBK->nAcceptedEvents(),
	   p_->orig_sumOfWeights        += allEventsCBK->sumOfEventWeights(),
	   p_->orig_sumOfWeightsSquared += allEventsCBK->sumOfEventWeightsSquared());
    } else {
      Error("QuickVBF", "No StreamAOD CutBookKeeper found");
      return EL::StatusCode::FAILURE;
    }
  } else {
    Error("fileExecute()", "Running on raw xAOD is not supported");
    //It actually does work, though, so keep going without failure
  }
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode QuickVBF::changeInput (bool /*firstFile*/) {
  RCU_CHANGE_INVARIANT(this);
  return EL::StatusCode::SUCCESS;
}

static void countNPV(WB& wb) {
  auto const * const vx = safeRetrieve<xAOD::VertexContainer>(wb.xAODEvent, "PrimaryVertices"); 
  wb.nPV = 0;
  wb.nV = 0;
  for (const auto vtx : *vx) {
    wb.nV++;
    if (vtx->vertexType() == xAOD::VxType::PriVtx) {
      wb.nPV++;
    }
  }
}

EL::StatusCode QuickVBF::initialize() {
  RCU_CHANGE_INVARIANT(this);

  const std::string sample = wk()->metaData()->castString("sample_name");
  p_->isData = isSampleData( sample );
  p_->isMC   = isSampleMC(   sample );
  RCU_ASSERT(p_->isData != p_->isMC);  
 
  std::unique_ptr<ana::QuickAna> quickAna(new ana::QuickAna("quickana_" + sample));
  quickAna->setConfig(this->cfg.qaConf);
  quickAna->isDataFlag = p_->isData; 
  for (const std::string& dir : cfg.dirGRL) {
    quickAna->muDataFiles.push_back(findMuData(dir));
  }
 
  for (const auto& s : quickAna->muDataFiles) {
    std::cout << "MYPRW " << s << std::endl;
  }
  
  quickAna->muMcFiles = findMuMC(this->cfg.dirPRW);
  Info("", "\n\nInitializing QuickAna\n\n");
  RCU_ASSERT(quickAna->initialize().isSuccess());
  Info("", "\n\nFinished initializing MY QuickAna\n\n");

  if (p_->isMC) {
    for (auto s : CP::make_systematics_vector(quickAna->recommendedSystematics())) {
      if (s.name().find("FT_EFF") == std::string::npos && s.name().find("MET_SoftTrk") == std::string::npos) {
	p_->variations.push_back( s );
      }
    }
  } else {
    p_->variations.push_back( CP::SystematicSet() );
  }

  if (p_->isMC) {
    CP::SystematicSet foo;
    // Only useful when running with histograms
    // foo.insert(CP::SystematicVariation("NO_WEIGHTS"));
    // foo.insert(CP::SystematicVariation("NO_WEIGHT_TRIGGER"));
    // foo.insert(CP::SystematicVariation("NO_WEIGHT_RECO"));
    // foo.insert(CP::SystematicVariation("NO_WEIGHT_PILEUP"));
    // foo.insert(CP::SystematicVariation("NO_WEIGHT_NJET"));
    // foo.insert(CP::SystematicVariation("NO_WEIGHT_MC"));
    for (auto s : CP::make_systematics_vector( foo )) {
      if (s.name() != "") {
	p_->variations.push_back( s ); 
      }
    }
  }

  RCU_ASSERT(quickAna->setSystematics( p_->variations ).isSuccess());
  p_->quickAna = std::move( quickAna );

  p_->cutGRL.initialize(this->cfg);

  std::cout << "Running systematics:" << std::endl;
  for (auto sys : p_->quickAna->systematics()) {
    std::cout << sys.name() << std::endl;
  }

  p_->cutflows = vbfCuts();
  for (auto s : p_->variations) {
    for (auto& flow : p_->cutflows) {
      prepare(systematicID(s), flow, p_->histStore);
    }
  }

  if (this->cfg.writeNtupleNominal) {
    p_->ntupleWriter.reset(new NtupleWriter(new VBFNtuple)); 
    TTree* tree = new TTree ("physics_nominal", "physics_nominal");
    tree->SetDirectory(wk()->getOutputFile("nominal"));
    p_->ntupleWriter->initialize(tree); 
  }
  
  if (this->cfg.writeNtupleSystematics) {
    for (const auto& s : p_->variations) {
      SysID sys = systematicID(s);
      const std::string treeName = "physics_" + static_cast<std::string>(sys);
      auto writer = astd::make_unique<NtupleWriter>(new VBFNtuple);
      TTree* tree = new TTree(treeName.c_str(), treeName.c_str());
      tree->SetDirectory(wk()->getOutputFile("systematics"));
      writer->initialize(tree);
      p_->ntupleWriters.insert(std::make_pair(sys, std::move(writer)));
     }
   } 
  
  p_->initialized = true;  
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode QuickVBF::execute() {
  RCU_CHANGE_INVARIANT(this);

  p_->nEvents++;

  if (p_->nEvents % 1000 == 0) {
    //produced some output to avoid long grid jobs being killed by pilot 
    std::cout << p_->nEvents << " events" << std::endl;
  }

  if (p_->isMC) {
    xAOD::EventInfo const * ei = nullptr;
    RCU_ASSERT(wk()->xaodEvent()->retrieve(ei, "EventInfo").isSuccess());
    p_->sumEventWeights += (ei->mcEventWeights().size() ? ei->mcEventWeights().at(0) : 1.);
  }

  if (this->cfg.use2jPreSelection) {
    const xAOD::JetContainer* jets = 0;
    RCU_ASSERT(wk()->xaodEvent()->retrieve(jets, "AntiKt4EMTopoJets").isSuccess());
    if (jets->size() < 2) { return EL::StatusCode::SUCCESS; }
    //2017-06-4 Disable the below as it is applied before OLR
    // if (jets->at(0)->pt() < 70000) { return EL::StatusCode::SUCCESS; }
    // if (jets->at(1)->pt() < 40000) { return EL::StatusCode::SUCCESS; }
    // if (TMath::Abs(jets->at(0)->eta() - jets->at(1)->eta()) < 2.0) { return EL::StatusCode::SUCCESS; }
    // //TODO: Remove this if running with MET
    // if ((jets->at(0)->p4()+jets->at(1)->p4()).Pt() < 90000) { return EL::StatusCode::SUCCESS; }
  }
  
  for (auto sys : p_->quickAna->systematics()) {
    if (p_->quickAna->applySystematicVariation(sys) != CP::SystematicCode::Ok) {
      return EL::StatusCode::FAILURE;
    }

    const SysID sysID = systematicID(sys);
   
    WB wb;
    wb.systematic = sysID;
    wb.xAODEvent = wk()->xaodEvent();
    wb.ei = safeRetrieve<xAOD::EventInfo>(wb.xAODEvent, "EventInfo"); 
    wb.passGRL = (p_->isMC ? true : p_->cutGRL.pass(wb));
    countNPV(wb);
    if (wb.nPV && wb.passGRL) {
      RCU_ASSERT(p_->quickAna->process(astd::ref(wk()->xaodEvent())).isSuccess());
      wb.qa = p_->quickAna.get();
      wb.vbfEvent = ::analyze(*wb.xAODEvent, *wb.qa);
      wb.vbfWeights = ::calcWeights(*wb.xAODEvent, *wb.qa, *wb.vbfEvent, sysID);
    }

    bool acceptEvent = false;
    for (auto& flow : p_->cutflows) {
      bool pass = process(sysID, flow, wb, p_->histStore);
      if (flow.writeNtuple && pass) { acceptEvent = true; }
    }

    if (this->cfg.writeNtupleNominal && std::string(sysID) == "nominal" && acceptEvent) {
      p_->ntupleWriter->process(wb); 
    }
    if (this->cfg.writeNtupleSystematics && acceptEvent) {
      p_->ntupleWriters.at(sysID)->process(wb); 
    }
    
  } 
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode QuickVBF::postExecute() {
  RCU_CHANGE_INVARIANT(this);
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode QuickVBF::finalize() {
  RCU_CHANGE_INVARIANT(this);

  xAOD::ReadStats& stats = xAOD::IOStats::instance().stats();
  if (false) stats.Print("SmartSlimming");

  for (auto s : p_->variations) {
    SysID sysID = systematicID(s);
    for (auto& flow : p_->cutflows) {

      if (not flow.systematics && std::string(sysID) != "nominal") { continue; }
      if (flow.hists.size() == 0 && not flow.writeNtuple) { continue; }
      
      FlowID flowID(flow.id);
      for (const HistVar& v : flow.hists) {
	VarID varID(v.name);
	wk()->addOutput(p_->histStore.release(sysID, flowID, varID));
      }
      wk()->addOutput(p_->histStore.release(sysID, flowID, VarID("CUTFLOW")));
    }
  } 
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode QuickVBF::histFinalize() {
  RCU_CHANGE_INVARIANT(this);
  wk()->addOutput(new TParameter<Double_t>("Cutflow/sumEventWeights",          p_->sumEventWeights         ));
  wk()->addOutput(new TParameter<Int_t>   ("Cutflow/eventsNoWeight",           p_->nEvents                 ));
  wk()->addOutput(new TParameter<Int_t>   ("Cutflow/DAOD_nEventsProcessed",    p_->orig_nEventsProcessed   ));
  wk()->addOutput(new TParameter<Double_t>("Cutflow/DAOD_sumOfWeights",        p_->orig_sumOfWeights       ));
  wk()->addOutput(new TParameter<Double_t>("Cutflow/DAOD_sumOfWeightsSquared", p_->orig_sumOfWeightsSquared));
  if (this->cfg.writeNtupleNominal) {
    TFile* file = wk()->getOutputFile("nominal");
    file->mkdir("Cutflow");
    file->cd("Cutflow");
    TParameter<Double_t>("sumEventWeights",          p_->sumEventWeights         ).Write();
    TParameter<Int_t>   ("eventsNoWeight",           p_->nEvents                 ).Write();
    TParameter<Int_t>   ("DAOD_nEventsProcessed",    p_->orig_nEventsProcessed   ).Write();
    TParameter<Double_t>("DAOD_sumOfWeights",        p_->orig_sumOfWeights       ).Write();
    TParameter<Double_t>("DAOD_sumOfWeightsSquared", p_->orig_sumOfWeightsSquared).Write();
    file->cd();
  }
  if (this->cfg.writeNtupleSystematics) {
    TFile* file = wk()->getOutputFile("systematics");
    file->mkdir("Cutflow");
    file->cd("Cutflow");
    TParameter<Double_t>("sumEventWeights",          p_->sumEventWeights         ).Write();
    TParameter<Int_t>   ("eventsNoWeight",           p_->nEvents                 ).Write();
    TParameter<Int_t>   ("DAOD_nEventsProcessed",    p_->orig_nEventsProcessed   ).Write();
    TParameter<Double_t>("DAOD_sumOfWeights",        p_->orig_sumOfWeights       ).Write();
    TParameter<Double_t>("DAOD_sumOfWeightsSquared", p_->orig_sumOfWeightsSquared).Write();
    file->cd();
  }
  return EL::StatusCode::SUCCESS;
}
