#include <QuickVBF/Triggers.h>
#include <QuickVBF/safeRetrieve.h>
#include <QuickAna/QuickAna.h>

#include <xAODTrigger/EnergySumRoI.h>
#include <xAODTrigMissingET/TrigMissingETContainer.h>
#include <QuickVBF/L1METvalue.h>

#include <TMath.h>

static bool isElTrigStr(const std::string& trigger) {
  return (trigger.find("HLT_e") != std::string::npos);
}

static bool isE2TrigStr(const std::string& trigger) {
  return (trigger.find("HLT_2e") != std::string::npos);
}

static bool isMuTrigStr(const std::string& trigger) {
  return (trigger.find("HLT_mu") != std::string::npos);
}

static bool isXETrigStr(const std::string& trigger) {
  return (trigger.find("HLT_xe") != std::string::npos);
}

static bool checkElTrigger(const std::string& trigger, xAOD::TEvent&, ana::IQuickAna& qa) {
  return qa.eventinfo()->auxdata<bool>(trigger+"_passTrig");
}

static bool checkE2Trigger(const std::string& trigger, xAOD::TEvent&, ana::IQuickAna& qa) {
  return qa.eventinfo()->auxdata<bool>(trigger+"_passTrig");
}

static bool checkMuTrigger(const std::string& trigger, xAOD::TEvent&, ana::IQuickAna& qa) {
  return qa.eventinfo()->auxdata<bool>(trigger+"_passTrig");
}

static bool checkXETrigger(const std::string& trigger, xAOD::TEvent& xv, ana::IQuickAna& qa) {

  if (qa.eventinfo()->eventType(xAOD::EventInfo::IS_SIMULATION)) {

    const auto* l1MetObject = safeRetrieve<xAOD::EnergySumRoI>(&xv,"LVL1EnergySumRoI"); 
    const float l1_mex = l1MetObject->exMiss();
    const float l1_mey = l1MetObject->eyMiss();
    float l1_met(0.);
    bool l1_overflow(false);
    static LVL1::L1METvalue l1calc;
    // Note that the L1METvalue object expects and returns values in GeV
    l1calc.calcL1MET(l1_mex/1000, l1_mey/1000, l1_met, l1_overflow);
    const bool passL1XE50 = l1_overflow || l1_met > 50;
    if (not passL1XE50) { return false; }
    
    const auto* hltCont = safeRetrieve<xAOD::TrigMissingETContainer>(&xv,"HLT_xAOD__TrigMissingETContainer_TrigEFMissingET_mht");   

    //if (hltCont->size() == 0) { return false; }

    const float ex = hltCont->front()->ex();
    const float ey = hltCont->front()->ey();
    const float hltMet = TMath::Sqrt(ex*ex + ey*ey);

    if (trigger == "HLT_xe70_mht")         { return (passL1XE50 && hltMet >  70000); }
    if (trigger == "HLT_xe90_mht_L1XE50")  { return (passL1XE50 && hltMet >  90000); }
    if (trigger == "HLT_xe110_mht_L1XE50") { return (passL1XE50 && hltMet > 110000); }
    throw "Don't know what kind of trigger this is: " + trigger;
    
  } else {
    return qa.eventinfo()->auxdata<bool>(trigger+"_passTrig");
  }

}

bool passTrigger(const std::string& trigger, xAOD::TEvent& xv, ana::IQuickAna& qa) {
  if (isElTrigStr(trigger)) { return checkElTrigger(trigger, xv, qa); }
  if (isE2TrigStr(trigger)) { return checkE2Trigger(trigger, xv, qa); }
  if (isMuTrigStr(trigger)) { return checkMuTrigger(trigger, xv, qa); }
  if (isXETrigStr(trigger)) { return checkXETrigger(trigger, xv, qa); }
  throw "Don't know what kind of trigger this is: " + trigger;
}
