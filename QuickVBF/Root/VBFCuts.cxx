
#include <QuickVBF/VBFCuts.h>
#include <QuickVBF/VBFHists.h>
#include <QuickVBF/WB.h>

#define CUT(X, Y) std::shared_ptr<IFlowItem>(new Cut(Y, [](const WB& wb)->bool{ return X; }))
#define CUTMC(X, Y) std::shared_ptr<IFlowItem>(new Cut(Y, [](const WB& wb)->bool{ return (wb.qa->eventinfo()->eventType(xAOD::EventInfo::IS_SIMULATION) ? X : true); }))
#define CUTDATA(X, Y) std::shared_ptr<IFlowItem>(new Cut(Y, [](const WB& wb)->bool{ return (wb.qa->eventinfo()->eventType(xAOD::EventInfo::IS_SIMULATION) ? true : X); }))
#define WGTMC(X, Y) std::shared_ptr<IFlowItem>(new Wgt(Y,  [](const WB& wb)->double{ return (wb.qa->eventinfo()->eventType(xAOD::EventInfo::IS_SIMULATION) ? X : 1.); }))

static void cart_product(std::vector<std::vector<Flow>>& rvv, 
			 std::vector<Flow>&  rv,
			 std::vector<std::vector<Flow>>::const_iterator me,
			 std::vector<std::vector<Flow>>::const_iterator end) {
  if(me == end) {
    rvv.push_back(rv);
    return;
  }
  const std::vector<Flow>& mev = *me;
  for(std::vector<Flow>::const_iterator it = mev.begin();
      it != mev.end();
      it++) {
    rv.push_back(*it);  
    cart_product(rvv, rv, me+1, end); 
    rv.pop_back(); 
  }
}

static std::vector<std::vector<Flow>> cart_product(std::vector<std::vector<Flow>>& in) {
  std::vector<std::vector<Flow>> rvv;
  std::vector<Flow> rv;
  cart_product(rvv, rv, in.begin(), in.end());
  return rvv;
}

static std::vector<std::vector<Flow>> cart_product2(std::vector<std::vector<Flow>>& in) {
  std::vector<std::vector<Flow>> rvv;
  std::vector<Flow> rv;
  for (auto end = in.end(); in.begin() != end; --end) {
    cart_product(rvv, rv, in.begin(), end);
  }
  return rvv;
}

static std::vector<Flow> expand_flows(std::vector<std::vector<Flow>>& in, bool partialFlows) {
  std::vector<Flow> r;
  std::vector<std::vector<Flow>> vv = (partialFlows ? cart_product2(in) : cart_product(in));
  for (auto v : vv) {
    Flow flow("");
    for (Flow& f : v) {
      flow.id += (flow.id.empty() ? "" : "_") + f.id;
      for (auto cut : f.items) {
	flow.items.push_back(cut);
      }
      flow.hists = f.hists; //intentional overwrite
      flow.systematics = f.systematics;
      flow.writeNtuple |= f.writeNtuple;
    }
    if (flow.hists.size() || flow.writeNtuple) {
      r.push_back(flow);
    }
  }
  return r;
}

std::vector<Flow> vbfCuts() {
  static const double MeV = 1.;
  static const double GeV = 1000.*MeV;
  static const double TeV = 1000.*GeV;

  Flow Pre("Pre");
  Pre.items.push_back(CUT(wb.passGRL,  "GRL"));
  Pre.items.push_back(CUT(wb.ei->errorState(xAOD::EventInfo::LAr)  != xAOD::EventInfo::Error, "LAr" ));
  Pre.items.push_back(CUT(wb.ei->errorState(xAOD::EventInfo::Tile) != xAOD::EventInfo::Error, "Tile"));
  Pre.items.push_back(CUT(wb.ei->errorState(xAOD::EventInfo::SCT)  != xAOD::EventInfo::Error, "SCT" ));
  Pre.items.push_back(CUT(not wb.ei->isEventFlagBitSet(xAOD::EventInfo::Core, 18),       "CoreFlags"));
  Pre.items.push_back(CUT(wb.nPV >= 1, "nPV > 1")); 
  
  Flow Weights("Weights", Pre.items);
  Weights.items.push_back(WGTMC(wb.vbfWeights->weightMC,     "MC weight"));
  Weights.items.push_back(WGTMC(wb.vbfWeights->weightNJet,   "NJet weight"));
  Weights.items.push_back(WGTMC(wb.vbfWeights->weightPileup, "Pileup weight"));
  Weights.items.push_back(WGTMC(wb.vbfWeights->weightReco,   "Reco weight"));
  Weights.hists = VBFHists::nV();
  Weights.systematics = false;
  
  Flow ElTrg("ElTrg");
  ElTrg.items.push_back(CUT(wb.vbfEvent->electrons.size()     >=  1,     ">= 1 el"));
  ElTrg.items.push_back(CUT(wb.vbfEvent->electrons[0]->pt()   >= 30*GeV, "e1 pT > 30 GeV"));
  ElTrg.items.push_back(CUT(wb.vbfWeights->elTrig,                       "el trigger"));
  ElTrg.items.push_back(WGTMC(wb.vbfWeights->weightElTrigSF,             "el trigger SF"));
  Flow MuTrg("MuTrg");
  MuTrg.items.push_back(CUT(wb.vbfEvent->muons.size()         >=  1,     ">= 1 mu"));
  MuTrg.items.push_back(CUT(wb.vbfEvent->muons[0]->pt()       >= 30*GeV, "m1 pT > 30 GeV"));
  MuTrg.items.push_back(CUT(wb.vbfWeights->muTrig,                       "mu trigger"));
  MuTrg.items.push_back(WGTMC(wb.vbfWeights->weightMuTrigSF,             "mu trigger SF"));
  Flow MuMetTrg("MuMetTrg");
  MuMetTrg.items.push_back(CUT(wb.vbfEvent->muons.size()         >=  1,     ">= 1 mu"));
  MuMetTrg.items.push_back(CUT(wb.vbfEvent->muons[0]->pt()       >= 30*GeV, "m1 pT > 30 GeV"));
  MuMetTrg.items.push_back(CUT(wb.vbfWeights->xeTrig,                       "MET trigger"));
  
  Flow Wev("Wev", ElTrg.items);
  Wev.items.push_back(CUT(wb.vbfEvent->electrons.size()  ==  1,     "== 1 el"));
  Wev.items.push_back(CUT(wb.vbfEvent->muons.size()      ==  0,     "== 0 mu"));
  Wev.items.push_back(CUT(wb.vbfEvent->met_lep_mt        >= 25*GeV, "M_T > 25 GeV"));
  Wev.hists = VBFHists::Wev();
  Wev.systematics = false;
  Flow Wnev("Wnev", Wev.items);
  Wnev.items.push_back(CUT(wb.vbfEvent->electrons[0]->charge() < 0, "el_charge < 0"));
  Flow Wpev("Wpev", Wev.items);
  Wpev.items.push_back(CUT(wb.vbfEvent->electrons[0]->charge() > 0, "el_charge > 0"));
  
  Flow Wmv("Wmv", MuTrg.items);
  Wmv.items.push_back(CUT(wb.vbfEvent->muons.size()      ==  1,     "== 1 mu"));
  Wmv.items.push_back(CUT(wb.vbfEvent->electrons.size()  ==  0,     "== 0 el"));
  Wmv.items.push_back(CUT(wb.vbfEvent->met_lep_mt        >= 25*GeV, "M_T > 25 GeV"));
  Wmv.hists = VBFHists::Wmv();
  Wmv.systematics = false;
  Flow Wnmv("Wnmv", Wmv.items);
  Wnmv.items.push_back(CUT(wb.vbfEvent->muons[0]->charge() < 0,     "mu_charge < 0"));
  Flow Wpmv("Wpmv", Wmv.items);
  Wpmv.items.push_back(CUT(wb.vbfEvent->muons[0]->charge() > 0,     "mu_charge > 0"));

  Flow MetWmv("MetWmv", MuMetTrg.items);
  MetWmv.items.push_back(CUT(wb.vbfEvent->muons.size()      ==  1,     "== 1 mu"));
  MetWmv.items.push_back(CUT(wb.vbfEvent->electrons.size()  ==  0,     "== 0 el"));
  MetWmv.items.push_back(CUT(wb.vbfEvent->met_lep_mt        >= 25*GeV, "M_T > 25 GeV"));
  MetWmv.hists = VBFHists::Wmv();
  Flow MetWnmv("MetWnmv", MetWmv.items);
  MetWnmv.items.push_back(CUT(wb.vbfEvent->muons[0]->charge() < 0,     "mu_charge < 0"));
  Flow MetWpmv("MetWpmv", MetWmv.items);
  MetWpmv.items.push_back(CUT(wb.vbfEvent->muons[0]->charge() > 0,     "mu_charge > 0"));
  
  Flow WevNoMT("WevNoMT", ElTrg.items);
  WevNoMT.items.push_back(CUT(wb.vbfEvent->electrons.size()  ==  1,     "== 1 el"));
  WevNoMT.items.push_back(CUT(wb.vbfEvent->muons.size()      ==  0,     "== 0 mu"));
  WevNoMT.hists = VBFHists::Wev();
  WevNoMT.systematics = false;
  Flow WnevNoMT("WnevNoMT", Wev.items);
  WnevNoMT.items.push_back(CUT(wb.vbfEvent->electrons[0]->charge() < 0, "el_charge < 0"));
  Flow WpevNoMT("WpevNoMT", Wev.items);
  WpevNoMT.items.push_back(CUT(wb.vbfEvent->electrons[0]->charge() > 0, "el_charge > 0"));
  
  Flow WmvNoMT("WmvNoMT", MuTrg.items);
  WmvNoMT.items.push_back(CUT(wb.vbfEvent->muons.size()      ==  1,     "== 1 mu"));
  WmvNoMT.items.push_back(CUT(wb.vbfEvent->electrons.size()  ==  0,     "== 0 el"));
  WmvNoMT.hists = VBFHists::Wmv();
  WmvNoMT.systematics = false;
  Flow WnmvNoMT("WnmvNoMT", Wmv.items);
  WnmvNoMT.items.push_back(CUT(wb.vbfEvent->muons[0]->charge() < 0,     "mu_charge < 0"));
  Flow WpmvNoMT("WpmvNoMT", Wmv.items);
  WpmvNoMT.items.push_back(CUT(wb.vbfEvent->muons[0]->charge() > 0,     "mu_charge > 0"));

  Flow MetWmvNoMT("MetWmvNoMT", MuMetTrg.items);
  MetWmvNoMT.items.push_back(CUT(wb.vbfEvent->muons.size()      ==  1,     "== 1 mu"));
  MetWmvNoMT.items.push_back(CUT(wb.vbfEvent->electrons.size()  ==  0,     "== 0 el"));
  MetWmvNoMT.hists = VBFHists::Wmv();
  Flow MetWnmvNoMT("MetWnmvNoMT", MetWmv.items);
  MetWnmvNoMT.items.push_back(CUT(wb.vbfEvent->muons[0]->charge() < 0,     "mu_charge < 0"));
  Flow MetWpmvNoMT("MetWpmvNoMT", MetWmv.items);
  MetWpmvNoMT.items.push_back(CUT(wb.vbfEvent->muons[0]->charge() > 0,     "mu_charge > 0"));

  
  Flow Zee("Zee", ElTrg.items);
  Zee.items.push_back(CUT(wb.vbfEvent->electrons.size()     ==  2, "== 2 el"));
  Zee.items.push_back(CUT(wb.vbfEvent->muons.size()         ==  0, "== 0 mu"));
  Zee.items.push_back(CUT(std::abs(wb.vbfEvent->z.M() - 91187) < 25*GeV, "|M(ll)-M_Z| < 25 GeV"));
  Zee.items.push_back(CUT(wb.vbfEvent->electrons[0]->charge()*wb.vbfEvent->electrons[1]->charge()<0,"OS"));
  Zee.hists = VBFHists::Zee();
  Zee.systematics = false;
  
  Flow Zmm("Zmm", MuTrg.items);
  Zmm.items.push_back(CUT(wb.vbfEvent->muons.size()     ==  2, "== 2 mu"));
  Zmm.items.push_back(CUT(wb.vbfEvent->electrons.size() ==  0, "== 0 el"));
  Zmm.items.push_back(CUT(std::abs(wb.vbfEvent->z.M() - 91187) < 25*GeV, "|M(ll)-M_Z| < 25 GeV"));
  Zmm.items.push_back(CUT(wb.vbfEvent->muons[0]->charge()*wb.vbfEvent->muons[1]->charge()<0,"OS"));  
  Zmm.hists = VBFHists::Zmm();
  Zmm.systematics = false;


  Flow MetZmm("MetZmm", MuMetTrg.items);
  MetZmm.items.push_back(CUT(wb.vbfEvent->muons.size()     ==  2, "== 2 mu"));
  MetZmm.items.push_back(CUT(wb.vbfEvent->electrons.size() ==  0, "== 0 el"));
  MetZmm.items.push_back(CUT(std::abs(wb.vbfEvent->z.M() - 91187) < 25*GeV, "|M(ll)-M_Z| < 25 GeV"));
  MetZmm.items.push_back(CUT(wb.vbfEvent->muons[0]->charge()*wb.vbfEvent->muons[1]->charge()<0,"OS"));  
  MetZmm.hists = VBFHists::Zmm();
  
  Flow MET("MET");
  MET.items.push_back(CUT(wb.vbfWeights->xeTrig,                       "MET trigger"));
  MET.items.push_back(CUT(wb.vbfEvent->electrons.size() ==  0       ,  "== 0 mu"));
  MET.items.push_back(CUT(wb.vbfEvent->muons.size()     ==  0       ,  "== 0 el"));
  MET.items.push_back(CUT(wb.vbfEvent->jets.size()      >=  2       &&
		          wb.vbfEvent->jets[0]->pt()    >= 50*GeV   &&
			  wb.vbfEvent->jets[1]->pt()    >= 35*GeV   &&
  			  wb.vbfEvent->jj_deta          >   2.5     ,  "DAODskim"));
  MET.items.push_back(CUTDATA( false , "BLIND"));
  
  Flow MyTwoJets("2j");
  MyTwoJets.items.push_back(CUT(wb.vbfEvent->jets.size()   >=  2    , ">= 2 jets"));
  MyTwoJets.items.push_back(CUT(wb.vbfEvent->jets[0]->pt() >= 80*GeV, "j1 pT > 80 GeV"));
  MyTwoJets.items.push_back(CUT(wb.vbfEvent->jets[1]->pt() >= 50*GeV, "j2 pT > 50 GeV"));
  MyTwoJets.items.push_back(CUT(wb.vbfEvent->jets.size()   ==  2,     "== 2 jets"));
  MyTwoJets.hists = VBFHists::JJ();
  MyTwoJets.writeNtuple = true;
  MyTwoJets.systematics = false;
  
  Flow DeltaEtaDeltaPhi("dEtadPhi");
  //DeltaEtaDeltaPhi.items.push_back(CUT(wb.vbfEvent->jj_deta > 4.8, "dEta(jj) > 4.8"));
  DeltaEtaDeltaPhi.items.push_back(CUT(wb.vbfEvent->jets[0]->eta() * wb.vbfEvent->jets[1]->eta() < 0, "jj opp. hemi."));
  DeltaEtaDeltaPhi.items.push_back(CUT(wb.vbfEvent->jj_dphi < 1.8, "dPhi(jj) < 1.8")); //2.5
  DeltaEtaDeltaPhi.hists = VBFHists::JJ();
  DeltaEtaDeltaPhi.systematics = false;
  
  Flow HTL("LEPMET");  
  HTL.items.push_back(CUT(wb.vbfEvent->lepmet.Mod()    > 150*GeV, "LEPMET > 150 GeV"));
  HTL.items.push_back(CUT(wb.vbfEvent->lepmet_jet_dphi >   1,     "LEPMETdPhi > 1"));
  
  Flow JJPT("JJPT");  
  JJPT.items.push_back(CUT(wb.vbfEvent->jj.Pt() > 150*GeV, "Pt(jj) > 150 GeV"));
  JJPT.systematics = false;
  
  Flow CR("CR");
  CR.items.push_back(CUT(wb.vbfEvent->jj_deta     >  2.5,     "dEta(jj) > 2.5"));
  CR.items.push_back(CUT(wb.vbfEvent->jj.M()      <= 1.0*TeV, "M(jj) < 1 TeV"));
  CR.hists = VBFHists::nEvt();
  
  Flow S1("S1");
  S1.items.push_back(CUT(wb.vbfEvent->jj_deta     >  4.8,     "dEta(jj) > 4.8"));
  S1.items.push_back(CUT(wb.vbfEvent->jj.M()      >= 1.0*TeV, "M(jj) > 1 TeV"));
  S1.items.push_back(CUT(wb.vbfEvent->jj.M()      <= 1.5*TeV, "M(jj) < 1.5 TeV"));
  S1.hists = VBFHists::nEvt();

  Flow S2("S2");
  S2.items.push_back(CUT(wb.vbfEvent->jj_deta     >  4.8,     "dEta(jj) > 4.8"));
  S2.items.push_back(CUT(wb.vbfEvent->jj.M()      >= 1.5*TeV, "M(jj) > 1.5 TeV"));
  S2.items.push_back(CUT(wb.vbfEvent->jj.M()      <= 2.0*TeV, "M(jj) < 2.0 TeV"));
  S2.hists = VBFHists::nEvt();

  Flow S3("S3");
  S3.items.push_back(CUT(wb.vbfEvent->jj_deta     >  4.8,     "dEta(jj) > 4.8"));
  S3.items.push_back(CUT(wb.vbfEvent->jj.M()      >= 2.0*TeV, "M(jj) > 2 TeV"));
  S3.hists = VBFHists::nEvt();

  Flow SR("SR");
  SR.items.push_back(CUT(wb.vbfEvent->jj_deta     >  4.8,     "dEta(jj) > 4.8"));
  SR.items.push_back(CUT(wb.vbfEvent->jj.M()      >= 1.0*TeV, "M(jj) > 1 TeV"));
  SR.hists = VBFHists::nEvt();


  
  Flow Unified("skim");
  Unified.items.push_back(CUT(wb.passGRL,  "GRL"));
  Unified.items.push_back(CUT(wb.ei->errorState(xAOD::EventInfo::LAr)  != xAOD::EventInfo::Error, "LAr" ));
  Unified.items.push_back(CUT(wb.ei->errorState(xAOD::EventInfo::Tile) != xAOD::EventInfo::Error, "Tile"));
  Unified.items.push_back(CUT(wb.ei->errorState(xAOD::EventInfo::SCT)  != xAOD::EventInfo::Error, "SCT" ));
  Unified.items.push_back(CUT(not wb.ei->isEventFlagBitSet(xAOD::EventInfo::Core, 18),       "CoreFlags"));
  Unified.items.push_back(CUT(wb.nPV >= 1, "nPV > 1")); 
  Unified.items.push_back(WGTMC(wb.vbfWeights->weightMC,     "MC weight"));
  Unified.items.push_back(WGTMC(wb.vbfWeights->weightNJet,   "NJet weight"));
  Unified.items.push_back(WGTMC(wb.vbfWeights->weightPileup, "Pileup weight"));
  Unified.items.push_back(WGTMC(wb.vbfWeights->weightReco,   "Reco weight"));
  Unified.items.push_back(CUT(wb.vbfEvent->jets.size()   >=  2    , ">= 2 jets"));
  Unified.items.push_back(CUT(wb.vbfEvent->jets[0]->pt() >= 70*GeV, "j1 pT > 70 GeV"));
  Unified.items.push_back(CUT(wb.vbfEvent->jets[1]->pt() >= 40*GeV, "j2 pT > 40 GeV"));
  //TODO: Remove this when running with MET
  Unified.items.push_back(CUT(wb.vbfEvent->jj.Pt() > 100*GeV, "Pt(jj) > 100 GeV"));
  Unified.writeNtuple = true;
  

  
  std::vector<std::vector<Flow>> cuts {
    // {Weights},
    // {//Wev, Wnev, Wpev, Wmv, Wnmv, Wpmv,
    //  WevNoMT, WnevNoMT, WpevNoMT, WmvNoMT, WnmvNoMT, WpmvNoMT, MetWmvNoMT, MetWnmvNoMT, MetWpmvNoMT,
    //    Zee, Zmm, MetZmm, MET},
    // {MyTwoJets},
    // {DeltaEtaDeltaPhi},
    // {/*HTL,*/ JJPT},
    // {CR, S1, S2, S3, SR},

    {Unified}
    
  };
  
  return expand_flows(cuts, true);  
}
