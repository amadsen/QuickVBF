#include <QuickVBF/TriggerPeriods.h>
#include <QuickVBF/utils.h>

#include <map>

namespace {
  
  enum struct Period  { a = 1, b = 2, c =3 };
  enum struct Trigger { el, e2, mu, xe }; 

  Period triggerPeriod(const unsigned runNumber) {
    if (runNumber < 289496) { return Period::a; }
    if (runNumber < 304008) { return Period::b; }
    return Period::c;
  }

  std::map<std::pair<Trigger, Period>, std::vector<std::string>> mkMap() {
    std::map<std::pair<Trigger, Period>, std::vector<std::string>> map;
    map[std::make_pair(Trigger::el, Period::a)] = {"HLT_e24_lhmedium_L1EM20VH",      "HLT_e60_lhmedium",      "HLT_e120_lhloose"}; 
    map[std::make_pair(Trigger::el, Period::b)] = {"HLT_e24_lhtight_nod0_ivarloose", "HLT_e60_lhmedium_nod0", "HLT_e140_lhloose_nod0"};
    map[std::make_pair(Trigger::el, Period::c)] = {                                  "HLT_e60_lhmedium_nod0", "HLT_e140_lhloose_nod0"};
    map[std::make_pair(Trigger::e2, Period::a)] = {"HLT_2e12_lhloose_L1EM10VH"};
    map[std::make_pair(Trigger::e2, Period::b)] = {"HLT_2e17_lhvloose_nod0"};
    map[std::make_pair(Trigger::e2, Period::c)] = {"HLT_2e17_lhvloose_nod0"};
    map[std::make_pair(Trigger::mu, Period::a)] = {"HLT_mu20_iloose_L1MU15_OR_HLT_mu50"};
    map[std::make_pair(Trigger::mu, Period::b)] = {"HLT_mu26_ivarmedium_OR_HLT_mu50"};
    map[std::make_pair(Trigger::mu, Period::c)] = {"HLT_mu26_ivarmedium_OR_HLT_mu50"};
    map[std::make_pair(Trigger::xe, Period::a)] = {"HLT_xe70_mht"};
    map[std::make_pair(Trigger::xe, Period::b)] = {"HLT_xe90_mht_L1XE50"}; 
    map[std::make_pair(Trigger::xe, Period::c)] = {"HLT_xe110_mht_L1XE50"};
    return map;
  }
  static const std::map<std::pair<Trigger, Period>, std::vector<std::string>> map = mkMap();
}

//el trigger, period c: {"HLT_e26_lhtight_nod0_ivarloose", "HLT_e60_lhmedium_nod0", "HLT_e140_lhloose_nod0"}; }
//Use e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0
//for ALL of 2016 when HLT_e26_lhtight_nod0_ivarloose available in derivation
//https://twiki.cern.ch/twiki/bin/view/AtlasProtected/LatestRecommendationsElectronIDRun2
//For now, use HLT_e24_lhtight_nod0_ivarloose up to D4 (and corresponding SF for <=1e34 menu)
//and no trigger below e60 for second part of 2016 
//Note: Check that QuickAna loads SF file for e26 

int triggerPeriodCategory(const unsigned runNumber) {
  return static_cast<int>(triggerPeriod(runNumber));
}

std::set<std::string> triggers2015() {
  std::set<std::string> s;
  for (const auto t : {Trigger::el, Trigger::e2, Trigger::mu, Trigger::xe}) {
    astd::insert(s, map.at(std::make_pair(t, Period::a)));
  }
  return s;
}

std::set<std::string> triggers2016() {
  std::set<std::string> s;
  for (const auto t : {Trigger::el, Trigger::e2, Trigger::mu, Trigger::xe}) {
    astd::insert(s, map.at(std::make_pair(t, Period::b)));
    astd::insert(s, map.at(std::make_pair(t, Period::c)));
  }
  return s;
}

std::set<std::string> triggers20152016() {
  std::set<std::string> r;
  astd::insert(r, triggers2015());
  astd::insert(r, triggers2016());
  return r;
}

std::vector<std::string> elTrigger(const unsigned runNumber) {
  return map.at(std::make_pair(Trigger::el, triggerPeriod(runNumber)));
}

std::vector<std::string> e2Trigger(const unsigned runNumber) {
  return map.at(std::make_pair(Trigger::e2, triggerPeriod(runNumber)));
}

std::vector<std::string> muTrigger(const unsigned runNumber) {
  return map.at(std::make_pair(Trigger::mu, triggerPeriod(runNumber)));
}

std::vector<std::string> metTrigger(const unsigned runNumber) {
  return map.at(std::make_pair(Trigger::xe, triggerPeriod(runNumber)));
}
