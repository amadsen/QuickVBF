#include <QuickVBF/HistStore.h>
#include <TH1.h>
#include <unordered_map>
#include <string>

namespace {
  struct HistKey {
    SysID systematic;
    FlowID flow;
    VarID var;
    HistKey(const SysID& systematic,
	    const FlowID& flow,
	    const VarID& var)
      : systematic(systematic)
      , flow(flow)
      , var(var) {}
    bool operator==(const HistKey& other) const {
      return (systematic == other.systematic &&
	            flow == other.flow       &&
	             var == other.var        );
    }
  };
  struct HistKeyHasher {
    std::size_t operator()(const HistKey& k) const {
      const size_t h1(std::hash<std::string>()(std::string(k.systematic)));
      const size_t h2(std::hash<std::string>()(std::string(k.flow)));
      const size_t h3(std::hash<std::string>()(std::string(k.var)));
      return h1 ^ h2 ^ h3; 
    }
  };
}
  
struct HistStore::Impl {
  std::unordered_map<HistKey, std::unique_ptr<TH1>, HistKeyHasher> hists;
};

HistStore::HistStore() : p_(new HistStore::Impl) {}

HistStore::~HistStore() {}

void HistStore::set(const SysID sys, const FlowID flow, const VarID var, TH1* h) {
  p_->hists[HistKey(sys, flow, var)].reset( h );    
}

TH1* HistStore::get(const SysID sys, const FlowID flow, const VarID var) {
  return p_->hists[HistKey(sys, flow, var)].get();
}

TH1* HistStore::release(const SysID sys, const FlowID flow, const VarID var) {
  return p_->hists[HistKey(sys, flow, var)].release();    
}
