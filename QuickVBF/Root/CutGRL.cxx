#include <QuickVBF/CutGRL.h>
#include <QuickVBF/safeRetrieve.h>
#include <QuickVBF/QVBFConfig.h>
#include <QuickVBF/WB.h>
#include <GoodRunsLists/GoodRunsListSelectionTool.h>
#include <RootCoreUtils/Assert.h>
#include <TSystem.h>
#include <vector>

namespace {
  using GRL = GoodRunsListSelectionTool;
}

static std::string findGRL(const std::string& dirGRL) {
  return gSystem->ExpandPathName( (dirGRL + "/GRL.xml").c_str() );
}

struct CutGRL::Impl {
  ~Impl() = default;
  std::unique_ptr<GRL> grl; 
};

CutGRL::CutGRL() : p_(new CutGRL::Impl) {}
CutGRL::~CutGRL() {}

std::string CutGRL::name() const { return "GRL"; }

void CutGRL::initialize(const QVBFConfig& cfg) {
  std::unique_ptr<GRL> grl{new GRL("QVBF_CutGRL_GoodRunsListSelectionTool")};
  std::vector<std::string> listOfGRL;
  for (const std::string& dir : cfg.dirGRL) {
    listOfGRL.push_back(findGRL(dir));
  }
  
  RCU_ASSERT(grl->setProperty("GoodRunsListVec", listOfGRL ).isSuccess());
  RCU_ASSERT(grl->setProperty("PassThrough", false).isSuccess());
  RCU_ASSERT(grl->initialize().isSuccess());
  p_->grl = std::move( grl );
}

bool CutGRL::pass(const WB& wb) const {
  if (p_->grl) {
    return p_->grl->passRunLB(*safeRetrieve<xAOD::EventInfo>(wb.xAODEvent, "EventInfo"));
  } else {
    return true;
  }
}
